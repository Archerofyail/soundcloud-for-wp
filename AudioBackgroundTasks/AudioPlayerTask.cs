﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using Windows.ApplicationModel.Background;
using Windows.Foundation.Collections;
using Windows.Media;
using Windows.Media.Playback;

namespace AudioBackgroundTasks
{

	public sealed class AudioPlayerTask : IBackgroundTask
	{
		private BackgroundTaskDeferral deferral;
		private MediaPlayer player;
		private SystemMediaTransportControls controls;

		private string[] titleStrings;
		private string[] streamUrlStrings;
		private string[] artistStrings;
		private int currentIndex = 0;

		public void Run(IBackgroundTaskInstance taskInstance)
		{
			Debug.WriteLine("started task");
			deferral = taskInstance.GetDeferral();

			taskInstance.Canceled += TaskInstance_Canceled;
			taskInstance.Task.Completed += TaskCompleted;

			player = BackgroundMediaPlayer.Current;
			player.CurrentStateChanged += OnMediaStateChanged;
			BackgroundMediaPlayer.MessageReceivedFromForeground += MessageReceived;
			var data = new ValueSet {{"SendData", ""}};
			BackgroundMediaPlayer.SendMessageToForeground(data);
			player.AutoPlay = true;
			player.MediaEnded += SongEnded;
			player.MediaOpened += MediaOpened;


			controls = SystemMediaTransportControls.GetForCurrentView();
			controls.ButtonPressed += MediaControlButtonPressed;
			controls.PropertyChanged += ControlsPropertyChanged;
			controls.IsEnabled = true;
			controls.IsPauseEnabled = true;
			controls.IsPlayEnabled = true;
			controls.IsPreviousEnabled = true;
			controls.IsNextEnabled = true;
		}

		private void MediaOpened(MediaPlayer sender, object args)
		{
			sender.Play();
			Debug.WriteLine("media opened, playing...");
		}

		private void OnMediaStateChanged(MediaPlayer sender, object args)
		{
			switch (sender.CurrentState)
			{
				case MediaPlayerState.Closed:
				{
					Debug.WriteLine("media player state is closed");
					controls.PlaybackStatus = MediaPlaybackStatus.Closed;
					break;
				}
				case MediaPlayerState.Opening:
				{
					Debug.WriteLine("media player state is opening");

					controls.PlaybackStatus = MediaPlaybackStatus.Changing;

					break;
				}
				case MediaPlayerState.Buffering:
				{
					Debug.WriteLine("media player state is buffering");

					controls.PlaybackStatus = MediaPlaybackStatus.Changing;

					break;
				}
				case MediaPlayerState.Playing:
				{
					Debug.WriteLine("media player state is Playing");

					controls.PlaybackStatus = MediaPlaybackStatus.Playing;
					UpdateUVCOnNewTrack();
					break;
				}
				case MediaPlayerState.Paused:
				{
					Debug.WriteLine("media player state is paused");

					controls.PlaybackStatus = MediaPlaybackStatus.Paused;

					break;
				}
				case MediaPlayerState.Stopped:
				{
					Debug.WriteLine("media player state is stopped");

					controls.PlaybackStatus = MediaPlaybackStatus.Stopped;

					break;
				}
				default:
				throw new ArgumentOutOfRangeException();
			}
		}

		private void TaskCompleted(BackgroundTaskRegistration sender, BackgroundTaskCompletedEventArgs args)
		{
			BackgroundMediaPlayer.Shutdown();
			deferral.Complete();
		}

		private void ControlsPropertyChanged(SystemMediaTransportControls sender,
			SystemMediaTransportControlsPropertyChangedEventArgs args)
		{

		}

		private void MediaControlButtonPressed(SystemMediaTransportControls sender,
			SystemMediaTransportControlsButtonPressedEventArgs args)
		{
			Debug.WriteLine("Button pressed");
			switch (args.Button)
			{
				case SystemMediaTransportControlsButton.Play:
				{
					player.Play();
					var data = new ValueSet();
					data.Add("isPlaying", true);
					BackgroundMediaPlayer.SendMessageToForeground(data);
					break;
				}
				case SystemMediaTransportControlsButton.Pause:
				{
					player.Pause();
					var data = new ValueSet();
					data.Add("isPlaying", false);
					BackgroundMediaPlayer.SendMessageToForeground(data);
					break;
				}
				case SystemMediaTransportControlsButton.Stop:
				{
					streamUrlStrings = null;
					artistStrings = null;
					titleStrings = null;
					BackgroundMediaPlayer.Shutdown();
					deferral.Complete();
					break;
				}
				case SystemMediaTransportControlsButton.Next:
				{
					MoveToNextSong();
					break;
				}
				case SystemMediaTransportControlsButton.Previous:
				{
					MoveToPreviousSong();
					break;
				}
				default:
				throw new ArgumentOutOfRangeException();
			}
		}

		private void SongEnded(MediaPlayer sender, object args)
		{
			MoveToNextSong();
		}

		private void MoveToPreviousSong()
		{
			var data = new ValueSet();
			data.Add("movetoprevious", "");
			BackgroundMediaPlayer.SendMessageToForeground(data);
			controls.PlaybackStatus = MediaPlaybackStatus.Changing;

			if (currentIndex == 0)
			{
				currentIndex = streamUrlStrings.Length - 1;
			}
			else
			{
				currentIndex--;
			}
			player.SetUriSource(new Uri(streamUrlStrings[currentIndex]));
			UpdateUVCOnNewTrack();
		}

		public void MoveToNextSong()
		{
			var data = new ValueSet();
			data.Add("movetonext", "");
			BackgroundMediaPlayer.SendMessageToForeground(data);
			controls.PlaybackStatus = MediaPlaybackStatus.Changing;
			if (currentIndex == streamUrlStrings.Length - 1)
			{
				currentIndex = 0;
			}
			else
			{
				currentIndex++;
			}
			player.SetUriSource(new Uri(streamUrlStrings[currentIndex]));
			UpdateUVCOnNewTrack();
		}

		private void UpdateUVCOnNewTrack()
		{
			controls.DisplayUpdater.Type = MediaPlaybackType.Music;
			controls.DisplayUpdater.MusicProperties.Title = titleStrings[currentIndex];
			controls.DisplayUpdater.MusicProperties.Artist = artistStrings[currentIndex];
			controls.DisplayUpdater.Update();
		}

		private void MessageReceived(object sender, MediaPlayerDataReceivedEventArgs e)
		{
			foreach (string key in e.Data.Keys)
			{
				switch (key.ToLower())
				{
					case "movetoprevious":
					{
						MoveToPreviousSong();
						break;
					}

					case "movetonext":
					{
						MoveToNextSong();
						break;
					}
					case "shutdown":
					{
						//Debug.WriteLine("shutting down...");
						//BackgroundMediaPlayer.Shutdown();
						//deferral.Complete();
						break;
					}
					case "play":
					{
						if ((bool)e.Data[key])
						{
							player.Play();
							var data = new ValueSet();
							data.Add("playing", true);
							BackgroundMediaPlayer.SendMessageToForeground(data);
							Debug.WriteLine("played music because of button from foreground");

						}
						else
						{
							var data = new ValueSet();
							data.Add("playing", false);
							BackgroundMediaPlayer.SendMessageToForeground(data);
							player.Pause();
							Debug.WriteLine("paused music because of button from foreground");
						}
						break;
						}
					case "streamurl0":
					{
						var itemCount = e.Data.Count / 3;
						string[] urls = new string[itemCount];
						string[] titles = new string[itemCount];
						string[] artists = new string[itemCount];
						Debug.WriteLine("found streamurl0, parsing...");
						foreach (var key1 in e.Data.Keys)
						{
							if (key1.Contains("title"))
							{
								var num = int.Parse(Regex.Match(key1, "\\d+").Value);
								titles[num] = e.Data[key1] as string;
							}
							else if (key1.Contains("streamurl"))
							{
								var num = int.Parse(Regex.Match(key1, "\\d+").Value);
								urls[num] = e.Data[key1] as string;
							}
							else if (key1.Contains("artist"))
							{
								var num = int.Parse(Regex.Match(key1, "\\d+").Value);
								artists[num] = e.Data[key1] as string;
							}
							else if (key1.Contains("index"))
							{
								currentIndex = (int)e.Data[key1];
							}
						}
						streamUrlStrings = urls.ToArray();
						artistStrings = artists.ToArray();
						titleStrings = titles.ToArray();
						StartPlayback();
						break;
					}
				}
			}	
		}

		public void StartPlayback()
		{
			player.SetUriSource(
				new Uri(streamUrlStrings[currentIndex]));
			var data = new ValueSet();
			data.Add("isPlaying", true);
			BackgroundMediaPlayer.SendMessageToForeground(data);
			Debug.WriteLine("should've started playing music, sent mesage to foreground");
			//BackgroundMediaPlayer.Current.Play();
		}

		private void TaskInstance_Canceled(IBackgroundTaskInstance sender, BackgroundTaskCancellationReason reason)
		{
			controls.ButtonPressed -= MediaControlButtonPressed;
			controls.PropertyChanged -= ControlsPropertyChanged;
			//clear objects task cancellation can happen uninterrupted 
			BackgroundMediaPlayer.Shutdown();



			Debug.WriteLine("Task cancelled because " + reason);
			BackgroundMediaPlayer.MessageReceivedFromForeground -= MessageReceived;
			player.MediaEnded -= SongEnded;
			deferral.Complete();
		}
	}
}