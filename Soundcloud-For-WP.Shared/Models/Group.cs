﻿using System;
using Windows.Data.Json;
using Soundcloud_For_WP.Shared.Resources;

namespace Soundcloud_For_WP.Shared.Models
{
	class Group
	{
		public int ID { get; private set; }
		public string Uri { get; private set; }
		public DateTime CreatedAt { get; private set; }
		public string Permalink { get; private set; }
		public string PermalinkUrl { get; private set; }
		public string ArtworkUrl { get; private set; }
		public string Name { get; private set; }
		public string Description { get; private set; }
		public string ShortDescription { get; private set; }
		public JsonObject Creator { get; private set; }

		public Group()
		{

		}

		public Group(int id, string uri, DateTime createdAt, string permalink, string permalinkUrl, string artworkUrl,
			string name, string description, string shortDescription, JsonObject creator)
		{
			ID = id;
			Uri = uri;
			CreatedAt = createdAt;
			Permalink = permalink;
			PermalinkUrl = permalinkUrl;
			ArtworkUrl = artworkUrl;
			Name = name;
			Description = description;
			ShortDescription = shortDescription;
			Creator = creator;
		}

		public static Group CreateGroupFromJsonObject(JsonObject groupObject)
		{
			IJsonValue tempVal;
			var id = 0;
			if (groupObject.TryGetValue(APIStrings.GeneralStrings.ID, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.Number)
				{
					id = (int)tempVal.GetNumber();
				}
			}
			var uri = "";
			if (groupObject.TryGetValue(APIStrings.GeneralStrings.Uri, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.String)
				{
					uri = tempVal.GetString();
				}
			}
			DateTime createdAt;
			var year = 1;
			var month = 1;
			var day = 1;
			if (groupObject.TryGetValue(APIStrings.SoundStrings.ReleaseYear, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.Number)
				{
					year = (int)tempVal.GetNumber();
				}
			}
			if (groupObject.TryGetValue(APIStrings.SoundStrings.ReleaseMonth, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.Number)
				{
					month = (int)tempVal.GetNumber();
				}
			} if (groupObject.TryGetValue(APIStrings.SoundStrings.ReleaseDay, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.Number)
				{
					day = (int)tempVal.GetNumber();
				}
			}
			createdAt = new DateTime(year, month, day);
			var permalink = "";
			if (groupObject.TryGetValue(APIStrings.GeneralStrings.Permalink, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.String)
				{
					permalink = tempVal.GetString();
				}
			}

			var permalinkUrl = "";
			if (groupObject.TryGetValue(APIStrings.GeneralStrings.PermalinkUrl, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.String)
				{
					permalinkUrl = tempVal.GetString();
				}
			}
			var artworkUrl = "";
			if (groupObject.TryGetValue(APIStrings.SoundStrings.ArtworkUrl, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.String)
				{
					artworkUrl = tempVal.GetString();
				}
			}
			var name = "";
			if (groupObject.TryGetValue(APIStrings.GroupStrings.Name, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.String)
				{
					name = tempVal.GetString();
				}
			}
			var description = "";
			if (groupObject.TryGetValue(APIStrings.GeneralStrings.Description, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.String)
				{
					description = tempVal.GetString();
				}
			}
			var shortDescription = "";
			if (groupObject.TryGetValue(APIStrings.GroupStrings.ShortDescription, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.String)
				{
					shortDescription = tempVal.GetString();
				}
			}
			var creator = JsonObject.Parse("{\"username\": \"Default\"}");
			if (groupObject.TryGetValue(APIStrings.GroupStrings.Creator, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.Object)
				{
					creator = tempVal.GetObject();
				}
			}
			return new Group(id, uri, createdAt, permalink, permalinkUrl, artworkUrl, name, description, shortDescription,
				creator);
		}
	}
}
