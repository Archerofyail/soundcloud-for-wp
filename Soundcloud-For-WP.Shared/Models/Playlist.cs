﻿using System;
using Windows.ApplicationModel.DataTransfer;
using Windows.Data.Html;
using Windows.Data.Json;
using Soundcloud_For_WP.Shared.DataModels;
using Soundcloud_For_WP.Shared.Resources;

namespace Soundcloud_For_WP.Shared.Models
{
	public enum PlaylistType
	{
		EPSingle,
		Album,
		Compilation,
		ProjectFiles,
		Archive,
		Showcase,
		Demo,
		SamplePack,
		Other
	}

	internal class Playlist
	{
		// ReSharper disable NotAccessedField.Global
		public int ID { get; private set; }
		public DateTime CreatedAt { get; private set; }
		public int UserID { get; private set; }
		public JsonObject User { get; private set; }
		public string Title { get; private set; }
		public string Permalink { get; private set; }
		public string PermalinkUrl { get; private set; }
		public string Uri { get; private set; }
		public string Sharing { get; private set; }
		public EmbeddableBy EmbeddableBy { get; private set; }
		public string PurchaseUrl { get; private set; }
		public string ArtworkUrl { get; private set; }
		public string Description { get; private set; }
		public JsonObject Label { get; private set; }
		public TimeSpan Duration { get; private set; }
		public string Genre { get; private set; }
		public int SharedToCount { get; private set; }
		public string[] TagList { get; private set; }
		public int LabelId { get; private set; }
		public string LabelName { get; private set; }
		public int Release { get; private set; }
		public DateTime ReleaseDate { get; private set; }
		public bool Streamable { get; private set; }
		public bool Downloadable { get; private set; }
		public string EAN { get; private set; }
		public PlaylistType PlaylistType { get; private set; }
		public Sound[] Tracks { get; private set; }
		public bool IsUserFavourite { get; private set; }
		// ReSharper restore NotAccessedField.Global

		public Playlist() { }

		public Playlist(int id, DateTime createdAt, int userID, JsonObject user, string title, string permalink,
			string permalinkUrl, string uri, string sharing, EmbeddableBy embeddableBy, string purchaseUrl, string artworkUrl,
			string description, JsonObject label, TimeSpan duration, string genre, int sharedToCount, string[] tagList,
			int labelId, string labelName, int release, DateTime releaseDate, bool streamable, bool downloadable, string EAN,
			PlaylistType playlistType, Sound[] tracks, bool isUserFavourite)
		{
			ID = id;
			CreatedAt = createdAt;
			UserID = userID;
			User = user;
			Title = title;
			Permalink = permalink;
			PermalinkUrl = permalinkUrl;
			Uri = uri;
			Sharing = sharing;
			EmbeddableBy = embeddableBy;
			PurchaseUrl = purchaseUrl;
			ArtworkUrl = artworkUrl;
			Description = description;
			Label = label;
			Duration = duration;
			Genre = genre;
			SharedToCount = sharedToCount;
			TagList = tagList;
			LabelId = labelId;
			LabelName = labelName;
			Release = release;
			ReleaseDate = releaseDate;
			Streamable = streamable;
			Downloadable = downloadable;
			this.EAN = EAN;
			PlaylistType = playlistType;
			Tracks = tracks;
			IsUserFavourite = isUserFavourite;
		}

		public static Playlist CreatePlaylistFromJsonObject(JsonObject playlistObject)
		{
			IJsonValue tempVal;
			var id = 0;
			if (playlistObject.TryGetValue(APIStrings.GeneralStrings.ID, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.Number)
				{
					id = (int)tempVal.GetNumber();
				}
			}
			var createdAt = new DateTime();
			if (playlistObject.TryGetValue(APIStrings.SoundStrings.CreatedAt, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.String)
				{
					createdAt = DateTime.Parse(tempVal.GetString());
				}
			}
			var userID = 0;
			if (playlistObject.TryGetValue(APIStrings.GeneralStrings.UserID, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.Number)
				{
					userID = (int)tempVal.GetNumber();
				}
			}
			var user = JsonObject.Parse("{\"username\": \"default\"}");
			if (playlistObject.TryGetValue(APIStrings.GeneralStrings.User, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.Object)
				{
					user = tempVal.GetObject();
				}
			}
			var title = "";
			if (playlistObject.TryGetValue(APIStrings.SoundStrings.Title, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.String)
				{
					title = tempVal.GetString();
				}
			}
			var permalink = "";
			if (playlistObject.TryGetValue(APIStrings.GeneralStrings.Permalink, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.String)
				{
					permalink = tempVal.GetString();
				}
			}
			var permalinkUrl = "";
			if (playlistObject.TryGetValue(APIStrings.GeneralStrings.PermalinkUrl, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.String)
				{
					permalinkUrl = tempVal.GetString();
				}
			}
			var uri = "";
			if (playlistObject.TryGetValue(APIStrings.GeneralStrings.Uri, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.String)
				{
					uri = tempVal.GetString();
				}
			}
			var sharing = "";
			if (playlistObject.TryGetValue(APIStrings.SoundStrings.Sharing, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.String)
				{
					sharing = tempVal.GetString();
				}
			}
			var embeddableBy = (EmbeddableBy)0;
			if (playlistObject.TryGetValue(APIStrings.SoundStrings.EmbeddableBy, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.String)
				{
					var initialString = tempVal.GetString();

					embeddableBy =
						(EmbeddableBy)
							Enum.Parse(typeof(EmbeddableBy), initialString.Substring(0, 1).ToUpper() + initialString.Substring(1));
				}
			}
			var purchaseUrl = "";
			if (playlistObject.TryGetValue(APIStrings.SoundStrings.PurchaseUrl, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.String)
				{
					purchaseUrl = tempVal.GetString();
				}
			}
			var artworkUrl = "/Resources/DefaultImages/NoArtwork.png";
			if (playlistObject.TryGetValue(APIStrings.SoundStrings.ArtworkUrl, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.String)
				{
					artworkUrl = tempVal.GetString();
				}
			}
			var description = "";
			if (playlistObject.TryGetValue(APIStrings.GeneralStrings.Description, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.String)
				{
					description = HtmlUtilities.ConvertToText(tempVal.GetString());
				}
			}
			var label = JsonObject.Parse("{\"label_name\":\"default\"}");
			if (playlistObject.TryGetValue(APIStrings.SoundStrings.Label, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.Object)
				{
					label = tempVal.GetObject();
				}
			}
			var duration = new TimeSpan();
			if (playlistObject.TryGetValue(APIStrings.SoundStrings.Duration, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.Number)
				{
					duration = TimeSpan.FromMilliseconds((int)tempVal.GetNumber());
				}
			}
			var genre = "";
			if (playlistObject.TryGetValue(APIStrings.SoundStrings.Genre, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.String)
				{
					genre = tempVal.GetString();
				}
			}
			var sharedToCount = 0;
			if (playlistObject.TryGetValue(APIStrings.SoundStrings.SharedToCount, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.Number)
				{
					sharedToCount = (int)tempVal.GetNumber();
				}
			}
			string[] tagList = new[] { "" };
			if (playlistObject.TryGetValue(APIStrings.SoundStrings.TagList, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.String)
				{
					tagList = tempVal.GetString().Split(' ');
				}
			}
			var labelId = 0;
			if (playlistObject.TryGetValue(APIStrings.GeneralStrings.LabelId, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.Number)
				{
					labelId = (int)tempVal.GetNumber();
				}
			}
			var labelName = "";
			if (playlistObject.TryGetValue(APIStrings.GeneralStrings.LabelName, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.String)
				{
					labelName = tempVal.GetString();
				}
			}
			var release = 0;
			if (playlistObject.TryGetValue(APIStrings.SoundStrings.Release, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.Number)
				{
					release = (int)tempVal.GetNumber();
				}
			}

			#region Release Date

			var releaseDate = new DateTime();
			var year = 1;
			var month = 1;
			var day = 1;
			if (playlistObject.TryGetValue(APIStrings.SoundStrings.ReleaseYear, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.Number)
				{
					year = (int)tempVal.GetNumber();
				}
			}
			if (playlistObject.TryGetValue(APIStrings.SoundStrings.ReleaseMonth, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.Number)
				{
					month = (int)tempVal.GetNumber();
				}
			}
			if (playlistObject.TryGetValue(APIStrings.SoundStrings.ReleaseDay, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.Number)
				{
					day = (int)tempVal.GetNumber();
				}
			}
			releaseDate = new DateTime(year, month, day);

			#endregion

			var streamable = false;
			if (playlistObject.TryGetValue(APIStrings.SoundStrings.Streamable, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.Boolean)
				{
					streamable = tempVal.GetBoolean();
				}
			}
			var downloadable = false;
			if (playlistObject.TryGetValue(APIStrings.SoundStrings.Downloadable, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.Boolean)
				{
					downloadable = tempVal.GetBoolean();
				}
			}
			var EAN = "";
			if (playlistObject.TryGetValue(APIStrings.PlaylistStrings.EAN, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.String)
				{
					EAN = tempVal.GetString();
				}
			}
			var playlistType = (PlaylistType)0;
			if (playlistObject.TryGetValue(APIStrings.PlaylistStrings.PlaylistType, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.String)
				{
					var words = tempVal.GetString().Split(' ');
					var typeName = "";
					foreach (var word in words)
					{
						if (word.Length > 0)
						{
							typeName += word.Substring(0, 1).ToUpper() + word.Substring(1);
						}
					}
					if (!string.IsNullOrEmpty(typeName))
					{
						playlistType = (PlaylistType)Enum.Parse(typeof(PlaylistType), typeName, true);
					}
				}
			}
			//Debug.WriteLine("Adding tracks to playlist with name: " + title);
			var tracksJson = new JsonArray();
			if (playlistObject.TryGetValue("tracks", out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.Array)
				{
					tracksJson = tempVal.GetArray();
				}
			}
			//Debug.WriteLine("tracks json is " + tracksJson + "\n and count is " + tracksJson.Count);
			var tracks = new Sound[tracksJson.Count];
			int i = 0;
			foreach (var track in tracksJson)
			{
				//Debug.WriteLine("Adding track...");
				tracks[i] = Sound.CreateFromJsonObject(track.GetObject());
				i++;
			}
			//Debug.WriteLine("Finished adding tracks to playlist: " + title);

			var favourite = false;
			if (playlistObject.TryGetValue("user_favorite", out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.Boolean)
				{
					favourite = tempVal.GetBoolean();
				}
			}

			return new Playlist(id, createdAt, userID, user, title, permalink, permalinkUrl, uri, sharing, embeddableBy,
				purchaseUrl, artworkUrl, description, label, duration, genre, sharedToCount, tagList, labelId, labelName, release,
				releaseDate, streamable, downloadable, EAN, playlistType, tracks, favourite);
		}
	}
}