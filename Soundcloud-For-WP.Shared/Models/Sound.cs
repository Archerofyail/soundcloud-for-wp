﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using Windows.Data.Json;
using Soundcloud_For_WP.Shared.Resources;

namespace Soundcloud_For_WP.Shared.DataModels
{
	public enum EncodingState
	{
		Processing,
		Failed,
		Finished
	}

	public enum TrackLicence
	{
		NoRightsReserved,
		AllRightsReserved,
		CcBy,
		CcByNc,
		CcByNd,
		CcBySa,
		CcByNcNd,
		CcByNcSa,
	}

	public enum TrackType
	{
		Original,
		Remix,
		Live,
		Recording,
		Spoken,
		Podcast,
		Demo,
		InProgress,
		Stem,
		Loop,
		SoundEffect,
		Sample,
		Other
	}

	public enum EmbeddableBy
	{
		All,
		Me,
		None
	}

	public class Sound
	{

		public int ID { get; private set; }
		public DateTime CreatedAt { get; private set; }
		public int UserID { get; private set; }
		public JsonObject User { get; private set; }
		public string UserName { get; set; }
		public string Title { get; private set; }
		public string Permalink { get; private set; }
		public string PermalinkUrl { get; private set; }
		public string Uri { get; private set; }
		public string Sharing { get; private set; }
		public string EmbeddableBy { get; private set; }
		public string PurchaseUrl { get; private set; }
		public string ArtworkUrl { get; private set; }
		public string Description { get; private set; }
		public JsonObject Label { get; private set; }
		public int Duration { get; private set; }
		public string Genre { get; private set; }
		public int SharedToCount { get; private set; }
		public string[] TagList { get; private set; }
		public int LabelId { get; private set; }
		public string LabelName { get; private set; }
		public string Release { get; private set; }
		public DateTime ReleaseDate { get; private set; }
		public bool Streamable { get; private set; }
		public bool Downloadable { get; private set; }
		public EncodingState EncodingState { get; private set; }
		public TrackLicence TrackLicence { get; private set; }
		public TrackType TrackType { get; private set; }
		public string WaveFormUrl { get; private set; }
		public string DownloadUrl { get; private set; }
		public string StreamUrl { get; private set; }
		public string VideoUrl { get; private set; }
		public int BPM { get; private set; }
		public bool Commentable { get; private set; }
		public string ISRC { get; private set; }
		public string KeySignature { get; private set; }
		public int CommentCount { get; private set; }
		public int DownloadCount { get; private set; }
		public int PlaybackCount { get; private set; }
		public int FavouritingsCount { get; private set; }
		public string OriginalFormat { get; private set; }
		public int OriginalContentSize { get; private set; }
		public JsonObject CreatedWith { get; private set; }
		public bool UserFavourite { get; set; }

		public Sound(int id, DateTime createdAt, int userID, JsonObject user, string title, string permalink,
			string permalinkUrl, string uri, string sharing, string embeddableBy, string purchaseUrl, string artworkUrl,
			string description, JsonObject label, int duration, string genre, int sharedToCount, string[] tagList,
			int labelId, string labelName, string release, DateTime releaseDate, bool streamable, bool downloadable,
			EncodingState encodingState, TrackLicence trackLicence, TrackType trackType, string waveFormUrl, string downloadUrl,
			string streamUrl, string videoUrl, int bpm, bool commentable, string ISRC, string keySignature, int commentCount,
			int downloadCount, int playbackCount, int favouritingsCount, string originalFormat, int originalContentSize,
			JsonObject createdWith, bool userFavourite)
		{
			ID = id;
			CreatedAt = createdAt;
			UserID = userID;
			User = user;
			Title = title;
			Permalink = permalink;
			PermalinkUrl = permalinkUrl;
			Uri = uri;
			Sharing = sharing;
			EmbeddableBy = embeddableBy;
			PurchaseUrl = purchaseUrl;
			ArtworkUrl = artworkUrl;
			Description = description;
			Label = label;
			Duration = duration;
			Genre = genre;
			SharedToCount = sharedToCount;
			TagList = tagList;
			LabelId = labelId;
			LabelName = labelName;
			Release = release;
			ReleaseDate = releaseDate;
			Streamable = streamable;
			Downloadable = downloadable;
			EncodingState = encodingState;
			TrackLicence = trackLicence;
			TrackType = trackType;
			WaveFormUrl = waveFormUrl;
			DownloadUrl = downloadUrl;
			StreamUrl = streamUrl;
			VideoUrl = videoUrl;
			BPM = bpm;
			Commentable = commentable;
			this.ISRC = ISRC;
			KeySignature = keySignature;
			CommentCount = commentCount;
			DownloadCount = downloadCount;
			PlaybackCount = playbackCount;
			FavouritingsCount = favouritingsCount;
			OriginalFormat = originalFormat;
			OriginalContentSize = originalContentSize;
			CreatedWith = createdWith;
			UserFavourite = userFavourite;
			UserName = user.GetNamedString("username");
		}

		public Sound(string artworkUrl, JsonObject user, string title)
		{
			//_artworkUrl = "http://img1.wikia.nocookie.net/__cb20140425002842/monstercat/images/2/28/20130616112740!Monstercat_Logo-1-.png";
			//_user = "Monstercat";
			//_title = "Some song by some guy in constructor";
			ArtworkUrl = artworkUrl;
			User = user;
			Title = title;
			UserName = user.GetNamedString("username");
		}

		public Sound()
		{
			Title = "Default Sound Constructor";
			User = new JsonObject();
		}

		public static Sound CreateFromJsonObject(JsonObject jsonSound)
		{
			IJsonValue tempVal;
			var ID = (int)jsonSound.GetNamedNumber(APIStrings.GeneralStrings.ID);
			var CreatedAt = DateTime.Parse(jsonSound.GetNamedString(APIStrings.SoundStrings.CreatedAt));
			var UserID = (int)jsonSound.GetNamedNumber(APIStrings.GeneralStrings.UserID);
			var User = jsonSound.GetNamedObject(APIStrings.GeneralStrings.User);
			var Title = jsonSound.GetNamedString(APIStrings.SoundStrings.Title);
			var Permalink = jsonSound.GetNamedString(APIStrings.GeneralStrings.Permalink);
			var PermalinkUrl = jsonSound.GetNamedString(APIStrings.GeneralStrings.PermalinkUrl);
			var Uri = jsonSound.GetNamedString(APIStrings.GeneralStrings.Uri);
			var Sharing = jsonSound.GetNamedString(APIStrings.SoundStrings.Sharing);
			var EmbeddableBy = jsonSound.GetNamedString(APIStrings.SoundStrings.EmbeddableBy);
			var PurchaseUrl = "";
			if (jsonSound.TryGetValue(APIStrings.SoundStrings.PurchaseUrl, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.String)
				{
					PurchaseUrl = tempVal.GetString();
				}
			}
			var ArtworkUrl = "/Assets/DefaultImages/NoArtwork.png";
			if (jsonSound.TryGetValue(APIStrings.SoundStrings.ArtworkUrl, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.String)
				{
					ArtworkUrl = tempVal.GetString();
				}
			}
			var Description = "";
			if (jsonSound.TryGetValue(APIStrings.GeneralStrings.Description, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.String)
				{
					Description = tempVal.GetString();
				}
			}
			JsonObject Label = null;

			if (jsonSound.TryGetValue(APIStrings.SoundStrings.Label, out tempVal))
			{
				Label = tempVal.GetObject();
			}

			var Duration = (int)jsonSound.GetNamedNumber(APIStrings.SoundStrings.Duration);
			var Genre = "";
			if (jsonSound.TryGetValue(APIStrings.SoundStrings.Genre, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.String)
				{
					Genre = tempVal.GetString();
				}
			}
			var SharedToCount = 0;

			if (jsonSound.TryGetValue(APIStrings.SoundStrings.SharedToCount, out tempVal))
			{
				SharedToCount = (int)tempVal.GetNumber();
			}
			var tags = Regex.Match(jsonSound.GetNamedString(APIStrings.SoundStrings.TagList), "tag\\d\\s(.+)")
				.Captures.Cast<string>()
				.ToArray();
			var LabelId = 0;

			if (jsonSound.TryGetValue(APIStrings.GeneralStrings.LabelId, out tempVal))
			{
				if (tempVal.ValueType != JsonValueType.Null)
				{
					LabelId = (int)tempVal.GetNumber();
				}
			}
			var LabelName = "";
			if (jsonSound.TryGetValue(APIStrings.GeneralStrings.LabelName, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.String)
				{
					LabelName = tempVal.GetString();
				}
			}
			var release = "";
			if (jsonSound.TryGetValue(APIStrings.SoundStrings.Release, out tempVal))
			{
				if (tempVal.ValueType != JsonValueType.Null)
				{
					release = tempVal.GetString();
				}
			}
			var releaseDate = new DateTime();
			{
				int year = 1;
				if (jsonSound.TryGetValue(APIStrings.SoundStrings.ReleaseYear, out tempVal))
				{
					if (tempVal.ValueType != JsonValueType.Null)
					{
						year = (int)tempVal.GetNumber();
					}
				}
				int month = 1;
				if (jsonSound.TryGetValue(APIStrings.SoundStrings.ReleaseMonth, out tempVal))
				{
					if (tempVal.ValueType != JsonValueType.Null)
					{
						month = (int)tempVal.GetNumber();
					}
				}
				int day = 1;
				if (jsonSound.TryGetValue(APIStrings.SoundStrings.ReleaseDay, out tempVal))
				{
					if (tempVal.ValueType != JsonValueType.Null)
					{
						day = (int)tempVal.GetNumber();
					}
				}
				releaseDate = new DateTime(year, month, day);
			}
			var streamable = false;
			if (jsonSound.TryGetValue(APIStrings.SoundStrings.Streamable, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.Boolean)
				{
					streamable = tempVal.GetBoolean();
				}
			}
			var downloadable = false;
			if (jsonSound.TryGetValue(APIStrings.SoundStrings.Downloadable, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.Boolean)
				{
					downloadable = tempVal.GetBoolean();
				}
			}
			var stateString = jsonSound.GetNamedString(APIStrings.SoundStrings.State);
			var state = (EncodingState)
				Enum.Parse(typeof(EncodingState), stateString.Substring(0, 1).ToUpper() + stateString.Substring(1));
			#region license
			var license1 = jsonSound.GetNamedString(APIStrings.SoundStrings.License);
			var licenseSplitArr = license1.Split('-');
			var license2 = "";
			foreach (var word in licenseSplitArr)
			{
				license2 += word[0].ToString().ToUpper() + word.Substring(1);
			}

			var license = (TrackLicence)
				Enum.Parse(typeof(TrackLicence), license2);
			#endregion
			#region TrackType

			var type = TrackType.Other;
			if (jsonSound.TryGetValue(APIStrings.SoundStrings.TrackType, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.String)
				{
					var type1 = tempVal.GetString();
					var typeSplit = type1.Split(' ');
					var typeName = "";
					if (typeSplit.Length > 0)
					{
						foreach (var s in typeSplit)
						{
							if (s.Length > 0)
							{
								typeName += s[0].ToString().ToUpper() + s.Substring(1);
							}
						}
						if (!string.IsNullOrEmpty(typeName))
						{
							if (!Enum.TryParse(typeName, out type))
							{
								type = TrackType.Other;
							}
						}
					}
				}
			}



			#endregion
			var WaveFormUrl = jsonSound.GetNamedString(APIStrings.SoundStrings.WaveFormUrl);
			string DownloadUrl = "";
			if (jsonSound.TryGetValue(APIStrings.SoundStrings.DownloadUrl, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.String)
				{
					DownloadUrl = tempVal.GetString();
				}
			}
			var StreamUrl = "";
			if (jsonSound.TryGetValue(APIStrings.SoundStrings.StreamUrl, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.String)
				{
					StreamUrl = SoundCloudAPI.ClientAuthorizeUrl(tempVal.GetString());
				}
			}
			var VideoUrl = "";
			if (jsonSound.TryGetValue(APIStrings.SoundStrings.VideoUrl, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.String)
				{
					VideoUrl = tempVal.GetString();
				}
			}
			var bpm = 0;
			if (jsonSound.TryGetValue(APIStrings.SoundStrings.BPM, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.Number)
				{
					bpm = (int)tempVal.GetNumber();
				}
			}
			var commentable = false;
			if (jsonSound.TryGetValue(APIStrings.SoundStrings.Commentable, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.Boolean)
				{
					commentable = tempVal.GetBoolean();
				}
			}
			var ISRC = "";
			if (jsonSound.TryGetValue(APIStrings.SoundStrings.ISRC, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.String)
				{
					ISRC = tempVal.GetString();
				}
			}
			var KeySignature = "";
			if (jsonSound.TryGetValue(APIStrings.SoundStrings.KeySignature, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.String)
				{
					KeySignature = tempVal.GetString();
				}
			}
			var commentCount = 0;
			if (jsonSound.TryGetValue(APIStrings.SoundStrings.CommentCount, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.Number)
				{
					commentCount = (int)tempVal.GetNumber();
				}
			}
			var downloadCount = 0;
			if (jsonSound.TryGetValue(APIStrings.SoundStrings.DownloadCount, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.Number)
				{
					downloadCount = (int)tempVal.GetNumber();
				}
			}
			var playbackCount = 0;
			if (jsonSound.TryGetValue(APIStrings.SoundStrings.PlaybackCount, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.Number)
				{
					playbackCount = (int)tempVal.GetNumber();
				}
			}
			var favouritingsCount = 0;
			if (jsonSound.TryGetValue(APIStrings.SoundStrings.FavouritingsCount, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.Number)
				{
					favouritingsCount = (int)tempVal.GetNumber();
				}
			}
			var OriginalFormat = jsonSound.GetNamedString(APIStrings.SoundStrings.OriginalFormat);
			var originalContentSize = (int)jsonSound.GetNamedNumber(APIStrings.SoundStrings.OriginalContentSize);
			JsonObject CreatedWith = null;

			if (jsonSound.TryGetValue(APIStrings.SoundStrings.CreatedWith, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.Object)
				{
					CreatedWith = tempVal.GetObject();
				}
			}
			var userFavourite = false;
			if (jsonSound.TryGetValue(APIStrings.SoundStrings.UserFavourite, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.Boolean)
				{
					userFavourite = tempVal.GetBoolean();
				}
			}

			var sound = new Sound(ID, CreatedAt, UserID, User, Title, Permalink, PermalinkUrl, Uri, Sharing, EmbeddableBy,
				PurchaseUrl, ArtworkUrl, Description, Label, Duration, Genre, SharedToCount, tags, LabelId, LabelName, release,
				releaseDate, streamable, downloadable, state, license, type, WaveFormUrl, DownloadUrl, StreamUrl, VideoUrl, bpm,
				commentable, ISRC, KeySignature, commentCount, downloadCount, playbackCount, favouritingsCount, OriginalFormat,
				originalContentSize, CreatedWith, userFavourite);
			return sound;
		}

	}
}