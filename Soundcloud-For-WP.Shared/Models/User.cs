﻿using Windows.Data.Json;
using Soundcloud_For_WP.Shared.Resources;

namespace Soundcloud_For_WP.Shared.DataModels
{
	class User
	{
		public int id { get; private set; }
		public string permalink { get; private set; }
		public string userName { get; private set; }
		public string uri { get; private set; }
		public string permalinkUri { get; private set; }
		public string avatarUrl { get; private set; }
		public string country { get; private set; }
		public string fullName { get; private set; }
		public string city { get; private set; }
		public string description { get; private set; }
		public string discogsName { get; private set; }
		public string myspaceName { get; private set; }
		public string website { get; private set; }
		public string websiteTitle { get; private set; }
		public bool online { get; private set; }
		public int trackCount { get; private set; }
		public int playListCount { get; private set; }
		public int followersCount { get; private set; }
		public int followingsCount { get; private set; }
		public int publicFavouritesCount { get; private set; }

		public User(
			int id,
			string permalink,
			string username,
			string uri,
			string permalinkUri,
			string avatarUrl,
			string country,
			string fullName,
			string city,
			string description,
			string discogsName,
			string myspaceName,
			string website,
			string websiteTitle,
			bool online,
			int trackCount,
			int playListCount,
			int followersCount,
			int followingsCount,
			int publicFavouritesCount)
		{
			this.id = id;
			this.permalink = permalink;
			userName = username;
			this.uri = uri;
			this.permalinkUri = permalinkUri;
			this.avatarUrl = avatarUrl;
			this.country = country;
			this.fullName = fullName;
			this.city = city;
			this.description = description;
			this.discogsName = discogsName;
			this.myspaceName = myspaceName;
			this.website = website;
			this.websiteTitle = websiteTitle;
			this.online = online;
			this.trackCount = trackCount;
			this.playListCount = playListCount;
			this.followersCount = followersCount;
			this.followingsCount = followingsCount;
			this.publicFavouritesCount = publicFavouritesCount;
		}

		public User()
		{
			
		}

		public static User CreateUserFromJsonObject(JsonObject userObject)
		{
			
			IJsonValue tempVal;
			var id = 0;
			if (userObject.TryGetValue(APIStrings.GeneralStrings.ID, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.Number)
				{
					id = (int)tempVal.GetNumber();
				}
			}

			var permalink = "";
			if (userObject.TryGetValue(APIStrings.GeneralStrings.Permalink, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.String)
				{
					permalink = tempVal.GetString();
				}
			}

			var userName = "";
			if (userObject.TryGetValue(APIStrings.UserStrings.UserName, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.String)
				{
					userName = tempVal.GetString();
				}
			}

			var uri = "";
			if (userObject.TryGetValue(APIStrings.GeneralStrings.Uri, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.String)
				{
					uri = tempVal.GetString();
				}
			}

			var permalinkUri = "";
			if (userObject.TryGetValue(APIStrings.GeneralStrings.PermalinkUrl, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.String)
				{
					permalinkUri = tempVal.GetString();
				}
			}

			var avatarUrl = "";
			if (userObject.TryGetValue(APIStrings.UserStrings.AvatarUrl, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.String)
				{
					avatarUrl = tempVal.GetString();
				}
			}

			var country = "";
			if (userObject.TryGetValue(APIStrings.UserStrings.Country, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.String)
				{
					country = tempVal.GetString();
				}
			}

			var fullName = "";
			if (userObject.TryGetValue(APIStrings.UserStrings.FullName, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.String)
				{
					fullName = tempVal.GetString();
				}
			}


			var city = "";
			if (userObject.TryGetValue(APIStrings.UserStrings.City, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.String)
				{
					city = tempVal.GetString();
				}
			}

			var description = "";
			if (userObject.TryGetValue(APIStrings.GeneralStrings.Description, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.String)
				{
					description = tempVal.GetString();
				}
			}

			var discogsName = "";
			if (userObject.TryGetValue(APIStrings.UserStrings.DiscogsName, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.String)
				{
					discogsName = tempVal.GetString();
				}
			}

			var myspaceName = "";
			if (userObject.TryGetValue(APIStrings.UserStrings.MySpaceName, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.String)
				{
					myspaceName = tempVal.GetString();
				}
			}

			var website = "";
			if (userObject.TryGetValue(APIStrings.UserStrings.Website, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.String)
				{
					website = tempVal.GetString();
				}
			}

			var websiteTitle = "";
			if (userObject.TryGetValue(APIStrings.UserStrings.WebsiteTitle, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.String)
				{
					websiteTitle = tempVal.GetString();
				}
			}

			var online = false;
			if (userObject.TryGetValue(APIStrings.UserStrings.Online, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.Boolean)
				{
					online = tempVal.GetBoolean();
				}
			}

			var trackCount = 0;
			if (userObject.TryGetValue(APIStrings.UserStrings.TrackCount, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.Number)
				{
					trackCount = (int)tempVal.GetNumber();
				}
			}

			var playlistCount = 0;
			if (userObject.TryGetValue(APIStrings.UserStrings.PlaylistCount, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.Number)
				{
					playlistCount = (int)tempVal.GetNumber();
				}
			}

			var followersCount = 0;
			if (userObject.TryGetValue(APIStrings.UserStrings.FollowersCount, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.Number)
				{
					followersCount = (int)tempVal.GetNumber();
				}
			}

			var followingsCount = 0;
			if (userObject.TryGetValue(APIStrings.UserStrings.FollowingsCount, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.Number)
				{
					followingsCount = (int)tempVal.GetNumber();
				}
			}

			var publicFavouritesCount = 0;
			if (userObject.TryGetValue(APIStrings.UserStrings.PublicFavouritesCount, out tempVal))
			{
				if (tempVal.ValueType == JsonValueType.Number)
				{
					publicFavouritesCount = (int)tempVal.GetNumber();
				}
			}

			return new User(id, permalink, userName, uri, permalinkUri, avatarUrl, country, fullName, city, description,
				discogsName, myspaceName, website, websiteTitle, online, trackCount, playlistCount, followersCount, followingsCount,
				publicFavouritesCount);
		}
	}
}