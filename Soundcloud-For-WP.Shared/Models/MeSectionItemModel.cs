﻿namespace Soundcloud_For_WP.Models
{
	class MeSectionItemModel
	{
		public string name;
		public string extra;

		public MeSectionItemModel()
		{
			name = "default";
		}

		public MeSectionItemModel(string name, string extra)
		{
			this.name = name;
			this.extra = extra;
		}

		public MeSectionItemModel(string name)
		{
			this.name = name;
			extra = "";
		}
	}
}
