﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;

namespace Soundcloud_For_WP
{
	public class BoundObservableCollection<T, TSource> : ObservableCollection<T>
	{
		private readonly Func<TSource, T> _converter;
		private readonly Func<T, TSource, bool> _isSameSource;

		public BoundObservableCollection(
			ObservableCollection<TSource> source,
			Func<TSource, T> converter,
			Func<T, TSource, bool> isSameSource)
		{
			_source = source;
			_converter = converter;
			_isSameSource = isSameSource;

			// Copy items
			AddItems(_source);

			// Subscribe to the source's CollectionChanged event
			_source.CollectionChanged += _source_CollectionChanged;
		}

		public ObservableCollection<TSource> _source { get; private set; }

		private void AddItems(IEnumerable<TSource> items)
		{
			foreach (TSource sourceItem in items)
			{
				Add(_converter(sourceItem));
			}
		}

		private void _source_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
		{
			switch (e.Action)
			{
				case NotifyCollectionChangedAction.Add:
					AddItems(e.NewItems.Cast<TSource>());
					break;
				case NotifyCollectionChangedAction.Move:
					// Not sure what to do here...
					break;
				case NotifyCollectionChangedAction.Remove:
					foreach (TSource sourceItem in e.OldItems.Cast<TSource>())
					{
						T toRemove = this.First(item => _isSameSource(item, sourceItem));
						Remove(toRemove);
					}
					break;
				case NotifyCollectionChangedAction.Replace:
					for (int i = e.NewStartingIndex; i < e.NewItems.Count; i++)
					{
						this[i] = _converter((TSource) e.NewItems[i]);
					}
					break;
				case NotifyCollectionChangedAction.Reset:
					Clear();
					AddItems(_source);
					break;
			}
		}
	}
}