﻿namespace Soundcloud_For_WP.Shared.Resources
{
	static class APIStrings
	{
		#region General

		public static class GeneralStrings
		{
			public const string ID = "id";
			public const string Permalink = "permalink";
			public const string PermalinkUrl = "permalink_url";
			public const string UserID = "user_id";
			public const string User = "user";
			public const string Uri = "uri";
			public const string LabelId = "label_id";
			public const string LabelName = "label_name";
			public const string Description = "description";
		}

		#endregion

		#region Sound

		public static class SoundStrings
		{
			public const string CreatedAt = "created_at";
			public const string Title = "title";
			public const string Sharing = "sharing";
			public const string EmbeddableBy = "embeddable_by";
			public const string PurchaseUrl = "purchase_url";
			public const string ArtworkUrl = "artwork_url";
			
			public const string Label = "label";
			public const string Duration = "duration";
			public const string Genre = "genre";
			public const string SharedToCount = "shared_to_count";
			public const string TagList = "tag_list";
			public const string Release = "release";
			public const string ReleaseDay = "release_day";
			public const string ReleaseMonth = "release_month";
			public const string ReleaseYear = "release_year";
			public const string Streamable = "streamable";
			public const string Downloadable = "downloadable";
			public const string State = "state";
			public const string License = "license";
			public const string TrackType = "track_type";
			public const string WaveFormUrl = "waveform_url";
			public const string DownloadUrl = "download_url";
			public const string StreamUrl = "stream_url";
			public const string VideoUrl = "video_url";
			public const string BPM = "bpm";
			public const string Commentable = "commentable";
			public const string ISRC = "isrc";
			public const string KeySignature = "key_signature";
			public const string CommentCount = "comment_count";
			public const string DownloadCount = "download_count";
			public const string PlaybackCount = "playback_count";
			public const string FavouritingsCount = "favoritings_count";
			public const string OriginalFormat = "original_format";
			public const string OriginalContentSize = "original_content_size";
			public const string CreatedWith = "created_with";
			public const string AssetData = "asset_data";
			public const string UserFavourite = "user_favorite";
		}


		#endregion

		#region Playlist
		public static class PlaylistStrings
		{
			public const string EAN = "ean";
			public const string PlaylistType = "playlist_type";
		}
		#endregion

		#region GroupStrings

		public static class GroupStrings
		{
			public const string Name = "name";
			public const string Creator = "creator";
			public const string ShortDescription = "short_description";
		}
		#endregion


		#region Username

		public static class UserStrings
		{
			public const string UserName = "username";
			public const string AvatarUrl = "avatar_url";
			public const string Country = "country";
			public const string FullName = "full_name";
			public const string City = "city";
			public const string DiscogsName = "discogs-name";
			public const string MySpaceName = "myspace-name";
			public const string Website = "website";
			public const string WebsiteTitle = "website-title";
			public const string Online = "online";
			public const string TrackCount = "track_count";
			public const string PlaylistCount = "playlist_count";
			public const string FollowersCount = "followers_count";
			public const string FollowingsCount = "followings_count";
			public const string PublicFavouritesCount = "public_favourites_count";
		}

		#endregion
	}
}
