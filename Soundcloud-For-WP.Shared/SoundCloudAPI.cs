﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Windows.Data.Json;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.System;

namespace Soundcloud_For_WP.Shared
{
	public static class SoundCloudAPI
	{
		private const string clientID = "f7e7c31a4d3a2f24404b6224ae287b00";
		private const string secret = "630b8d6e03b0323be6bea11f8a8552a3";
		private const string clientIDParamString = "client_id";
		private const string tokenFileName = "Token";
		private static string OAuthToken;
		private static readonly HttpClient client = new HttpClient();
		private static readonly StorageFolder localFolder;
		public static bool isSignedIn;
		static SoundCloudAPI()
		{
			localFolder = ApplicationData.Current.LocalFolder;
			client.BaseAddress = new Uri("https://api.soundcloud.com");

		}

		public delegate void Authenticate();

		public static event Authenticate AuthenticateEvent;

		/// <summary>
		/// launches the web browser to let the user connect to soundcloud.
		/// The site then redirects to the app where it then Authenticates with the returned code.
		/// </summary>
		public static async void SignIn()
		{
			var signInParams = new List<KeyValuePair<string, string>> 
			{ 
				new KeyValuePair<string, string>(clientIDParamString, clientID),
				new KeyValuePair<string, string>("redirect_uri", "scfwp://soundcloud/callback"),
				new KeyValuePair<string, string>("response_type", "code"),
				new KeyValuePair<string, string>("scope", "non-expiring"),
				new KeyValuePair<string, string>("display", "popup"),
			};
			var stringConvert = await new FormUrlEncodedContent(signInParams).ReadAsStringAsync();
			await Launcher.LaunchUriAsync(new Uri("https://soundcloud.com/connect?" + stringConvert));
		}

		public static async void SignOut()
		{
			OAuthToken = "";
			isSignedIn = false;
			await (await localFolder.GetFileAsync(tokenFileName)).DeleteAsync();
			Debug.WriteLine("deleted token file successfully");
		}

		/// <summary>
		/// Posts to SoundCloud's oauth/token endpoint with the code to get authenticated.
		/// If authenticated, writes the returned token to file and sets isSignedIn to true.
		/// </summary>
		/// <param name="code"></param>
		/// <returns></returns>
		public static async Task OAuthenticate(string code)
		{
			Debug.WriteLine("Code is " + code);
			var authParams = new List<KeyValuePair<string, string>>
			{
				new KeyValuePair<string, string>(clientIDParamString, clientID),
				new KeyValuePair<string, string>("client_secret", secret),
				new KeyValuePair<string, string>("redirect_uri", "scfwp://soundcloud/callback"),
				new KeyValuePair<string, string>("grant_type", "authorization_code"),
				new KeyValuePair<string, string>("code", code)
			};
			var response = await client.PostAsync("/oauth2/token", new FormUrlEncodedContent(authParams));
			var jsonString = await response.Content.ReadAsStringAsync();
			Debug.WriteLine("string is " + jsonString);
			var oauthObj = JsonObject.Parse(jsonString);
			IJsonValue val;
			if (oauthObj.TryGetValue("access_token", out val))
			{
				OAuthToken = val.GetString();
			}
			else
			{
				Debug.WriteLine("Failed to authenticate with code: " + code);
			}
			if (!string.IsNullOrEmpty(OAuthToken))
			{
				await WriteTokenToFileAsync(OAuthToken);
				isSignedIn = true;
				if (AuthenticateEvent != null)
				{
					AuthenticateEvent();
				}
			}
		}

		/// <summary>
		/// Writes the user's OAuthToken to file and creates the file if it doesn't already exist
		/// </summary>
		/// <param name="token"></param>
		/// <returns></returns>
		private static async Task WriteTokenToFileAsync(string token)
		{
			Debug.WriteLine("Token is " + token + " before write");
			StorageFile file;
			var isThere = await FileExists();
			if (isThere)
			{
				file = await localFolder.GetFileAsync(tokenFileName);
			}
			else
			{
				file = await localFolder.CreateFileAsync(tokenFileName);
			}

			using (Stream inputStream = await file.OpenStreamForWriteAsync())
			{
				var outStream = inputStream.AsOutputStream();
				var dataWriter = new DataWriter(outStream)
				{
					ByteOrder = ByteOrder.BigEndian

				};
				var stringLength = dataWriter.MeasureString(token);
				dataWriter.WriteUInt32(stringLength);
				dataWriter.WriteString(token);
				await dataWriter.StoreAsync();
				await dataWriter.FlushAsync();
			}
		}

		/// <summary>
		/// Tries to read the token from the file it saves to. If successful it switched isSignedIn to true and sets the OAuthToken
		/// </summary>
		public static async Task ReadToken()
		{
			try
			{
				var file = await localFolder.GetFileAsync(tokenFileName);
				
				using (var inputStream = await file.OpenSequentialReadAsync())
				{


					
					var token = "";
					using (var dataReader = new DataReader(inputStream))
					{
						dataReader.ByteOrder = ByteOrder.BigEndian;

						await dataReader.LoadAsync(sizeof (uint));
						
						var stringSize = dataReader.ReadUInt32();
						
						await dataReader.LoadAsync(stringSize);
						token = dataReader.ReadString(stringSize);
						if (string.IsNullOrEmpty(token))
						{
							await file.DeleteAsync();
							isSignedIn = false;
						
							return;
						}
						
						OAuthToken = token;
						isSignedIn = true;
					}
				}

				
			}
			catch (Exception e)
			{
				Debug.WriteLine("File does not exist");
				Debug.WriteLine(e);
			}

		}

		private static async Task<string> PutAsync(string relativeUri, List<KeyValuePair<string, string>> parameters = null)
		{
			var putParams = new List<KeyValuePair<string, string>>();
			putParams.Add(new KeyValuePair<string, string>(clientIDParamString, clientID));
			if (parameters != null)
			{
				putParams.AddRange(parameters);
			}
			return
				await
					(await client.PutAsync("https://api.soundcloud.com" + relativeUri, new FormUrlEncodedContent(putParams))).Content
						.ReadAsStringAsync();
		}

		private static async Task<string> DeleteAsync(string relativeUri, List<KeyValuePair<string, string>> parameters = null)
		{
			var deleteParams = new List<KeyValuePair<string, string>>();
			deleteParams.Add(new KeyValuePair<string, string>(clientIDParamString, clientID));
			if (parameters != null)
			{
				deleteParams.AddRange(parameters);
			}
			var uri = new Uri("https://api.soundcloud.com" + relativeUri + "?" + 
			                  await new FormUrlEncodedContent(deleteParams).ReadAsStringAsync());
			var response =
				await
					client.DeleteAsync(uri).ConfigureAwait(false);
			return await response.Content.ReadAsStringAsync();
		}
		
		private static async Task<string> PostAsync(string relativeUri, List<KeyValuePair<string, string>> parameters = null)
		{
			var postParams = new List<KeyValuePair<string, string>>();
			postParams.Add(new KeyValuePair<string, string>(clientIDParamString, clientID));
			if (parameters != null)
			{
				postParams.AddRange(parameters);
			}
			var respone =
				await (client.PostAsync("http://api.soundcloud.com" + relativeUri, new FormUrlEncodedContent(postParams))).ConfigureAwait(false);
			return await respone.Content.ReadAsStringAsync();
		}

		/// <summary>
		/// The base Get method for the API. always appends the oauth_token and client_id parameters to the uri.
		/// </summary>
		/// <param name="relativeUri">The uri relative to http://api.soundcloud.com</param>
		/// <param name="optionalParams">The optional list of parameters that are appended to the uri</param>
		/// <param name="appendOAuthToken">Optionally append the user's oauth token to the get request. Default value is false</param>
		/// <returns>The json string returned by the API</returns>
		private static async Task<string> GetAsync(string relativeUri, List<KeyValuePair<string, string>> optionalParams = null, bool appendOAuthToken = false)
		{
			Debug.WriteLine("Doing Get Async base method");
			var requestParams = new List<KeyValuePair<string, string>>();
			
			requestParams.Add(new KeyValuePair<string, string>(clientIDParamString, clientID));
			if (appendOAuthToken)
			{
				requestParams.Add(new KeyValuePair<string, string>("oauth_token", OAuthToken));
			}
			if (optionalParams != null)
			{
				requestParams.AddRange(optionalParams);
			}
			var url = "https://api.soundcloud.com" + relativeUri + "?" +
			          await new FormUrlEncodedContent(requestParams).ReadAsStringAsync().ConfigureAwait(false);
			Debug.WriteLine("url is " + url);
			var response = await client.GetAsync(url).ConfigureAwait(false);
			Debug.WriteLine("Got response from " + url);
			return await response.Content.ReadAsStringAsync();
		}
		/// <summary>
		/// Searches for the searchString by the type and returns the amount of results specified by the limit. 
		/// Use the offset if you have already searched and want to get more results
		/// </summary>
		/// <param name="type">The type of object returned</param>
		/// <param name="searchString">The string to search for</param>
		/// <param name="limit">The amount of results to return at a time</param>
		/// <param name="offset">How many results you want to skip (used if you have already performed this search and want to get more results)</param>
		/// <returns>The json string of the results</returns>
		public async static Task<string> Search(string type, string searchString, int limit = 30, int offset = 0)
		{
			return
				await
					GetAsync("/" + type + ".json",
						new List<KeyValuePair<string, string>>(new[]
						{
							new KeyValuePair<string, string>("q", searchString),
							new KeyValuePair<string, string>("limit", limit.ToString()),
							new KeyValuePair<string, string>("offset", offset.ToString()),
						}), true).ConfigureAwait(false);
		}

		/// <summary>
		/// Resolves a user based on their username
		/// </summary>
		/// <param name="userName">The username to resolve</param>
		/// <returns>A string of Json if the username is found, otherwise it returns a 404</returns>
		public static async Task<string> ResolveUserId(string userName)
		{
			var user =
				await
					GetAsync("/resolve.json",
						new List<KeyValuePair<string, string>>(new[]
						{new KeyValuePair<string, string>("url", "http://soundcloud.com/" + userName)})).ConfigureAwait(false);
			//Debug.WriteLine("Username string is " + user);
			return JsonObject.Parse(user).GetNamedNumber("id").ToString();
		}

		#region GetData

		/// <summary>
		/// Gets the subresource of type for a user
		/// </summary>
		/// <param name="type">The type of subresource to get</param>
		/// <param name="userId">The id of the user</param>
		/// <returns>the json string of results, otherwise it returns a 404</returns>
		public static async Task<string> GetUserSubResource(string type, string userId)
		{
			var objectString = await GetAsync("/users/" + userId + "/" + type + ".json").ConfigureAwait(false);
			//Debug.WriteLine("tracks are " + tracksString);
			return objectString;
		}

		/// <summary>
		/// Gets a SoundCloud object of type by it's id
		/// </summary>
		/// <param name="type">The type of object to get (playlists, users, tracks, etc.)</param>
		/// <param name="id">The id of the item</param>
		/// <returns>The object in json as a string</returns>
		public static async Task<string> GetObjectById(string type, int id)
		{
			Debug.WriteLine("Getting object...");
			var result = await GetAsync("/" + type + "/" + id + ".json", null, true).ConfigureAwait(false);
			return result;
		}

		public static string ClientAuthorizeUrl(string url)
		{
			return url + "?" + clientIDParamString + "=" + clientID;
		}

		#endregion

		#region Me
		/// <summary>
		/// Get's the user's info, optionally getting a subresource using the relative Uri
		/// </summary>
		/// <param name="relativeUri">The optional uri relative to http://api.soundcloud.com/me.</param>
		/// <param name="requestParams">The optional parameters for the request</param>
		/// <returns>The user (or a subresource) in json as a string</returns>
		public static async Task<string> GetMe(string relativeUri = "", List<KeyValuePair<string, string>> requestParams = null)
		{
			Debug.WriteLine("token is " + OAuthToken);
			var parameters =
				new List<KeyValuePair<string, string>>(new[] {new KeyValuePair<string, string>("oauth_token", OAuthToken)});
			if (requestParams != null)
			{
				parameters.AddRange(requestParams);
			}
			var result =
				await
					client.GetAsync("/me" + relativeUri + ".json?" + await new FormUrlEncodedContent(parameters).ReadAsStringAsync()).ConfigureAwait(false);
			var resultString = await result.Content.ReadAsStringAsync();
			return resultString;
		}

		/// <summary>
		/// Gets a subresource of the authenticated user
		/// </summary>
		/// <param name="type">The type of subresource</param>
		/// <returns>A json string of the results, otherwise a 404</returns>
		public static async Task<string> GetMySubresource(string type)
		{
			return await GetMe("/" + type).ConfigureAwait(false);
		}

		/// <summary>
		/// Get's the authenticated user's stream
		/// </summary>
		/// <param name="limit">The optional limit of the amount of items to grab. The default is 30</param>
		/// <returns>The json string of the results</returns>
		public static async Task<string> GetStream(int limit = 30)
		{
			string result =
				await
					GetMe("/activities/all",
						new List<KeyValuePair<string, string>>(new[]
						{
							new KeyValuePair<string, string>("limit", limit.ToString())
						}))
						.ConfigureAwait(false);
			//Debug.WriteLine("response string is " + result);
			return result;
		}

		public static async Task<string> Like(int id)
		{
			var parameters = new List<KeyValuePair<string, string>>();
			parameters.Add(new KeyValuePair<string, string>("oauth_token", OAuthToken));
			return await PutAsync("/me/favorites/" + id + ".json", parameters);
		}

		public static async Task<string> Unlike(int id)
		{
			var parameters = new List<KeyValuePair<string, string>>();
			parameters.Add(new KeyValuePair<string, string>("oauth_token", OAuthToken));
			return await DeleteAsync("/me/favorites/" + id + ".json", parameters);
		}

		#endregion

		/// <summary>
		/// Checks if the token file exists
		/// </summary>
		/// <returns></returns>
		public static async Task<bool> FileExists()
		{
			bool exists = false;
			try
			{
				var files = await localFolder.GetFilesAsync();
				foreach (var file in files)
				{
					if (file.Name == tokenFileName)
					{
						exists = true;
					}
				}
			}
			catch (Exception)
			{
				exists = false;
			}
			return exists;
		}
	}
}
