﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Windows.Data.Json;
using Soundcloud_For_WP.Shared.DataModels;
using Soundcloud_For_WP.Shared.Models;
using Soundcloud_For_WP.ViewModels;

namespace Soundcloud_For_WP.Shared.ViewModels
{
	class UserViewModel : INotifyPropertyChanged
	{
		private User user;
		private bool isMe;

		private ObservableCollection<Sound> userTracks;
		private BoundObservableCollection<SoundViewModel, Sound> userTracksCollection;
		public BoundObservableCollection<SoundViewModel, Sound> UserTracks
		{
			get
			{
				if (userTracks == null)
				{
					userTracks = new ObservableCollection<Sound>();
					var tracksArray = new JsonArray();
					if (isMe)
					{
						if (!JsonArray.TryParse(SoundCloudAPI.GetUserSubResource("tracks", user.id.ToString()).Result, out tracksArray))
						{
							tracksArray = new JsonArray();
						}
					}
					else
					{
						if (!JsonArray.TryParse(SoundCloudAPI.GetMySubresource("tracks").Result, out tracksArray))
						{
							tracksArray = new JsonArray();
						}
					}
					foreach (var track in tracksArray)
					{
						userTracks.Add(Sound.CreateFromJsonObject(track.GetObject()));
					}
					userTracksCollection = new BoundObservableCollection<SoundViewModel, Sound>(userTracks,
						sound => new SoundViewModel(sound), (model, sound) => sound.ID == model.model.ID);
				}
				return userTracksCollection;
			}
		}

		private ObservableCollection<Playlist> playlists;
		private BoundObservableCollection<PlaylistViewModel, Playlist> playlistsCollection;
		public BoundObservableCollection<PlaylistViewModel, Playlist> Playlists
		{
			get
			{
				if (playlists == null)
				{
					playlists = new ObservableCollection<Playlist>();
					var playlistsArray = new JsonArray();
					if (isMe)
					{
						Debug.WriteLine("Checking if tracks have been received properly...");
						if (!JsonArray.TryParse(SoundCloudAPI.GetMySubresource("playlists").Result, out playlistsArray))
						{
							Debug.WriteLine("json failed to parse, creating empty array");
							playlistsArray = new JsonArray();
						}
					}
					else
					{
						if (!JsonArray.TryParse(SoundCloudAPI.GetUserSubResource("playlists", user.id.ToString()).Result, out playlistsArray))
						{
							playlistsArray = new JsonArray();
						}
					}
					foreach (var track in playlistsArray)
					{
						playlists.Add(Playlist.CreatePlaylistFromJsonObject(track.GetObject()));
					}
					playlistsCollection = new BoundObservableCollection<PlaylistViewModel, Playlist>(playlists,
						playlist => new PlaylistViewModel(playlist), (model, playlist) => playlist.ID == model.Playlist.ID);
				}
				return playlistsCollection;
			}
		}

		private ObservableCollection<User> followers;
		private BoundObservableCollection<UserViewModel, User> followersCollection;
		public BoundObservableCollection<UserViewModel, User> Followers
		{
			get
			{
				if (followers == null)
				{
					followers = new ObservableCollection<User>();
					var usersArray = new JsonArray();
					if (isMe)
					{
						if (!JsonArray.TryParse(SoundCloudAPI.GetMySubresource("followers").Result, out usersArray))
						{
							usersArray = new JsonArray();
						}
					}
					else
					{
						if (!JsonArray.TryParse(SoundCloudAPI.GetUserSubResource("followers", user.id.ToString()).Result, out usersArray))
						{
							usersArray = new JsonArray();
						}
					}
					foreach (var track in usersArray)
					{
						followers.Add(User.CreateUserFromJsonObject(track.GetObject()));
					}
					followersCollection = new BoundObservableCollection<UserViewModel, User>(followers,
						user1 => new UserViewModel(user1, false), (model, user1) => user1.id == model.User.id);
				}
				return followersCollection;
			}
		}

		private ObservableCollection<User> followings;
		private BoundObservableCollection<UserViewModel, User> followingsCollection;
		public BoundObservableCollection<UserViewModel, User> Followings
		{
			get
			{
				if (followings == null)
				{
					followings = new ObservableCollection<User>();
					var usersArray = new JsonArray();
					if (isMe)
					{
						if (!JsonArray.TryParse(SoundCloudAPI.GetMySubresource("followings").Result, out usersArray))
						{
							usersArray = new JsonArray();
						}
					}
					else
					{
						if (!JsonArray.TryParse(SoundCloudAPI.GetUserSubResource("followings", user.id.ToString()).Result, out usersArray))
						{
							usersArray = new JsonArray();
						}
					}
					foreach (var track in usersArray)
					{
						followings.Add(User.CreateUserFromJsonObject(track.GetObject()));
					}
					followingsCollection = new BoundObservableCollection<UserViewModel, User>(followings,
						user1 => new UserViewModel(user1, false), (model, user1) => user1.id == model.User.id);
				}
				return followingsCollection;
			}
		}

		private ObservableCollection<Sound> favourites;
		private BoundObservableCollection<SoundViewModel, Sound> favouritesCollection;
		public BoundObservableCollection<SoundViewModel, Sound> Favourites
		{
			get
			{
				if (favourites == null)
				{
					favourites = new ObservableCollection<Sound>();
					var jsonString = "";
					if (isMe)
					{
						jsonString = SoundCloudAPI.GetMySubresource("favorites").Result;
					}
					else
					{
						jsonString = SoundCloudAPI.GetUserSubResource("favorites", user.id.ToString()).Result;						
					}
					var usersArray = new JsonArray();
					if (!string.IsNullOrEmpty(jsonString))
					{
						 usersArray = JsonArray.Parse(jsonString);
					}
					foreach (var track in usersArray)
					{
						favourites.Add(Sound.CreateFromJsonObject(track.GetObject()));
					}
					favouritesCollection = new BoundObservableCollection<SoundViewModel, Sound>(favourites,
						sound => new SoundViewModel(sound), (model, sound) => sound.ID == model.model.ID);
				}
				return favouritesCollection;
			}
		}

		public User User
		{
			get { return user; }
			set
			{
				if (value != user)
				{
					user = value;
					foreach (var prop in typeof(UserViewModel).GetRuntimeProperties())
					{
						if (prop.PropertyType != typeof(ICommand) && prop.PropertyType != typeof(User))
						{
							OnPropertyChanged(prop.Name);
						}
					}
				}
			}
		}

		public string UserName
		{
			get { return user.userName; }
		}

		public string AvatarUrl
		{
			get { return user.avatarUrl.Replace("large", "t300x300"); }
		}

		public string Website
		{
			get { return user.website; }
		}

		public string WebsiteTitle
		{
			get { return !string.IsNullOrEmpty(user.website) ? "Go to the website" : "No Website"; }
		}

		public bool HasWebsite
		{
			get { return !string.IsNullOrEmpty(user.website); }
		}

		public string TrackCount
		{
			get { return user.trackCount.ToString(); }
		}

		public string FollowersCount
		{
			get { return user.followersCount.ToString(); }
		}

		public string Location
		{
			get { return user.city + (!string.IsNullOrEmpty(user.country) ? ", " : "") + user.country; }
		}

		public string Description
		{
			get { return user.description; }
		}

		public event PropertyChangedEventHandler PropertyChanged;

		public UserViewModel()
		{
			user = new User();
		}

		public UserViewModel(int id)
		{
			Debug.WriteLine("constructing user viewmodel with id of " + id);
			var jsonString = SoundCloudAPI.GetObjectById("users", id).Result;
			Debug.WriteLine("got user json string, it is " + jsonString);
			user = User.CreateUserFromJsonObject(JsonObject.Parse(jsonString));
			Debug.WriteLine("finished creating user in userviewmodel");
		}

		public UserViewModel(User user, bool isMe)
		{
			this.user = user;
			this.isMe = isMe;
		}

		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
