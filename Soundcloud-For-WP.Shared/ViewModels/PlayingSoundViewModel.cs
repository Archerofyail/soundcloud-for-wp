﻿using System;
using System.ComponentModel;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;
using Soundcloud_For_WP.Shared.DataModels;

namespace Soundcloud_For_WP.Shared.ViewModels
{
	internal class PlayingSoundViewModel : INotifyPropertyChanged
	{
		private Sound sound;

		private Sound Sound
		{
			get { return sound; }
			set
			{
				if (value != sound)
				{
					foreach (var property in typeof (PlayingSoundViewModel).GetRuntimeProperties())
					{
						sound = value;
						if (property.Name != "Sound" && property.Name != "MoveToPrevious" && property.Name != "MoveToNext" &&
						    property.Name != "Pause")
						{
							// ReSharper disable ExplicitCallerInfoArgument
							OnPropertyChanged(property.Name);
							// ReSharper restore ExplicitCallerInfoArgument
						}
					}
				}
			}
		}

		public string ArtworkUrl
		{
			get { return sound.ArtworkUrl.Replace("large", "crop"); }
			set
			{
				if (PropertyChanged != null)
				{
					OnPropertyChanged();
				}
			}
		}


		public string IsFavourite
		{
			get { return sound.UserFavourite ? "" : ""; }
		}

		public SolidColorBrush IsFavouriteColor
		{
			get
			{
				return
					new SolidColorBrush(Sound.UserFavourite
						? Color.FromArgb(Convert.ToByte(255), Convert.ToByte(255), Convert.ToByte(140), Convert.ToByte(0))
						: Application.Current.RequestedTheme == ApplicationTheme.Light
							? Color.FromArgb(Convert.ToByte(255), Convert.ToByte(0), Convert.ToByte(0), Convert.ToByte(0))
							: Color.FromArgb(Convert.ToByte(255), Convert.ToByte(255), Convert.ToByte(255), Convert.ToByte(255)));
			}
		}

		public string WaveFormUrl
		{
			get { return sound.WaveFormUrl; }
			set
			{
				if (PropertyChanged != null)
				{
					OnPropertyChanged();
				}
			}
		}

		public string StreamUrl
		{
			get { return sound.StreamUrl; }
			set
			{
				if (PropertyChanged != null)
				{
					OnPropertyChanged();
				}
			}
		}

		public string UserName
		{
			get { return sound.User.GetNamedString("username"); }
			set
			{
				if (PropertyChanged != null)
				{
					OnPropertyChanged();
				}
			}
		}

		public string Title
		{
			get { return sound.Title; }
			set
			{
				if (PropertyChanged != null)
				{
					OnPropertyChanged();
				}
			}
		}

		public string Favouritings
		{
			get { return sound.FavouritingsCount.ToString(); }
			set
			{
				if (PropertyChanged != null)
				{
					OnPropertyChanged();
				}
			}
		}

		public string Reposts
		{
			get { return sound.SharedToCount.ToString(); }
			set
			{
				if (PropertyChanged != null)
				{
					OnPropertyChanged();
				}
			}
		}

		public string CommentCount
		{
			get { return sound.CommentCount.ToString(); }
			set
			{
				if (PropertyChanged != null)
				{
					OnPropertyChanged();
				}
			}
		}

		public string ReleaseDate
		{
			get { return sound.CreatedAt.ToString("ddd MMM dd, yyyy"); }
			set
			{
				if (PropertyChanged != null)
				{
					OnPropertyChanged();
				}
			}
		}

		public string Duration
		{
			get { return TimeSpan.FromMilliseconds(sound.Duration).ToString("g"); }
			set
			{
				if (PropertyChanged != null)
				{
					OnPropertyChanged();
				}
			}
		}

		public string PlaybackCount
		{
			get { return Sound.PlaybackCount.ToString(); }
		}

		public string FavouritingsCount
		{
			get { return Sound.FavouritingsCount.ToString(); }
		}

		public bool IsDownloadable
		{
			get { return sound.Downloadable; }
		}

		public ICommand MoveToPrevious
		{
			get { return new RelayCommand(MovePreviousExecute); }
		}

		public ICommand MoveToNext
		{
			get { return new RelayCommand(MoveNextExecute); }
		}

		public ICommand FavouriteSong
		{
			get { return new RelayCommand(FavouriteSongExecute); }
		}

		public void MovePreviousExecute(object param)
		{
			Sound = PlaylistManager.MoveToPrevious();
		}

		public void MoveNextExecute(object param)
		{
			Sound = PlaylistManager.MoveToNext();
		}

		public async void FavouriteSongExecute(object param)
		{
			if (!sound.UserFavourite)
			{
				var response = await SoundCloudAPI.Like(sound.ID);
				if (response.Contains("Created"))
				{
					sound.UserFavourite = true;
					OnPropertyChanged("IsFavourite");
					OnPropertyChanged("IsFavouriteColor");
				}
			}
			else
			{
				var response = await SoundCloudAPI.Unlike(sound.ID);
				if (response.Contains("OK"))
				{
					sound.UserFavourite = false;
					OnPropertyChanged("IsFavourite");
					OnPropertyChanged("IsFavouriteColor");
				}
			}
		}

		public PlayingSoundViewModel() {}

		public PlayingSoundViewModel(Sound source)
		{
			sound = source;
		}

		public event PropertyChangedEventHandler PropertyChanged;

		private void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}