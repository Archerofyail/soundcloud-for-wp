﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Windows.UI;
using Windows.UI.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;
using Soundcloud_For_WP.Shared.DataModels;

namespace Soundcloud_For_WP.Shared.ViewModels
{
	class SoundViewModel : INotifyPropertyChanged
	{
		public string username;
		public string title;
		public string artworkUrl;
		public string waveFormUrl;
		public Sound model;
		public event PropertyChangedEventHandler PropertyChanged;

		public string Username
		{
			get { return username; }
			set
			{
				if (username != value)
				{
					username = value;
					OnPropertyChanged();
				}
			}
		}
		public string Title
		{
			get { return title; }
			set
			{
				if (title != value)
				{
					title = value;
					OnPropertyChanged();
				}
			}
		}
		public string ArtworkUrl
		{
			get { return artworkUrl.Replace("large", "t300x300"); }
			set
			{
				if (artworkUrl != value)
				{
					artworkUrl = value;
					OnPropertyChanged();
				}
			}
		}
		public string WaveFormUrl
		{
			get { return waveFormUrl; }
			set
			{
				if (waveFormUrl != value)
				{
					waveFormUrl = value;
					OnPropertyChanged();
				}
			}
		}

		public string PlaybackCount
		{
			get { return model.PlaybackCount.ToString(); }
		}

		public string FavouritingsCount
		{
			get { return model.FavouritingsCount.ToString(); }
		}

		public string SharedToCount
		{
			get { return model.SharedToCount.ToString(); }
		}

		public string CommentCount
		{
			get { return model.CommentCount.ToString(); }
		}

		public string ReleaseDate
		{
			get
			{
				var returnString = "";
				var span = DateTime.Now.Subtract(model.CreatedAt);
				if (Math.Floor(span.Days / 365f) > 0)
				{
					returnString = span.Days / 365 + ( span.Days / 365 == 1 ? " year" :" years");
				}
				else if (Math.Floor(span.Days / 30f) > 0)
				{
					returnString = span.Days / 30 + (span.Days / 30 == 1 ? " month" : " months");
				}
				else if (Math.Floor(span.Days / 7f) > 0)
				{
					returnString = span.Days / 7 + ((span.Days / 7) == 1 ? " week" : " weeks");
				}
				else if (span.Days > 0)
				{
					returnString = span.Days + (span.Days == 1 ? " day" : " days");
				}
				else if (span.TotalHours > 1)
				{
					returnString = span.Hours + (span.Hours == 1 ? " hour" : " hours");
				}
				else if (span.Minutes > 30)
				{
					returnString = "half an hour";
				}
				else if (span.Minutes > 1)
				{
					returnString = span.Minutes + " minutes";
				}
				else
				{
					returnString = "less than a minute";
				}
				return returnString;
			}
		}

		public bool IsDownloadable
		{
			get { return model.Downloadable; }
		}

		public Brush IsUserFavourite
		{
			get
			{
				return
					new SolidColorBrush(model.UserFavourite
						? Color.FromArgb(Convert.ToByte(255), Convert.ToByte(255), Convert.ToByte(140), Convert.ToByte(0))
						: Application.Current.RequestedTheme == ApplicationTheme.Light
							? Color.FromArgb(Convert.ToByte(255), Convert.ToByte(0), Convert.ToByte(0), Convert.ToByte(0))
							: Color.FromArgb(Convert.ToByte(255), Convert.ToByte(255), Convert.ToByte(255), Convert.ToByte(255)));
			}
		}

		public SoundViewModel()
		{
			Title = "Default ViewModel";
			username = "Default ViewModel";
		}

		public SoundViewModel(Sound source)
		{
			Title = source.Title;
			Username = source.UserName;
			ArtworkUrl = source.ArtworkUrl;
			WaveFormUrl = source.WaveFormUrl;
			model = source;
		}

		public void Pressed(object sender, TappedEventArgs args)
		{

		}

		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
