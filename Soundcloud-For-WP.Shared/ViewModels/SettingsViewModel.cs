﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Soundcloud_For_WP.Annotations;
using Soundcloud_For_WP.Shared;

namespace Soundcloud_For_WP.ViewModels
{
	internal class SettingsViewModel : INotifyPropertyChanged
	{
		public bool IsDisconnectAvailable
		{
			get { return SoundCloudAPI.isSignedIn; }
		}

		public ICommand Disconnect
		{
			get { return new RelayCommand(DisconnectExecute); }
		}

		public event PropertyChangedEventHandler PropertyChanged;

		public void DisconnectExecute(object param)
		{
			SoundCloudAPI.SignOut();
			OnPropertyChanged("IsDisconnectAvailable");
		}

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}