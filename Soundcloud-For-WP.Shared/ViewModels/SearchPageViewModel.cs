﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.Data.Json;
using Soundcloud_For_WP.Shared.DataModels;
using Soundcloud_For_WP.Shared.Models;
using Soundcloud_For_WP.ViewModels;
using Windows.UI.ViewManagement;


namespace Soundcloud_For_WP.Shared.ViewModels
{
	class SearchPageViewModel : INotifyPropertyChanged
	{

		private StatusBarProgressIndicator statusBar;

		private ObservableCollection<Sound> foundSounds = new ObservableCollection<Sound>();
		private BoundObservableCollection<SoundViewModel, Sound> foundSoundsCollection;
		public BoundObservableCollection<SoundViewModel, Sound> FoundSoundsCollection
		{
			get { return foundSoundsCollection; }
			set
			{
				if (value != foundSoundsCollection)
				{
					foundSoundsCollection = value;
					OnPropertyChanged();
				}
			}
		}

		private ObservableCollection<User> foundUsers = new ObservableCollection<User>();
		private BoundObservableCollection<UserViewModel, User> foundUsersCollection;
		public BoundObservableCollection<UserViewModel, User> FoundUsersCollection
		{
			get { return foundUsersCollection; }
			set
			{
				if (value != foundUsersCollection)
				{
					foundUsersCollection = value;
					OnPropertyChanged();
				}
			}
		}

		private ObservableCollection<Playlist> foundPlaylists = new ObservableCollection<Playlist>();
		private BoundObservableCollection<PlaylistViewModel, Playlist> foundPlaylistsCollection;
		public BoundObservableCollection<PlaylistViewModel, Playlist> FoundPlaylistsCollection
		{
			get { return foundPlaylistsCollection; }
			set
			{
				if (value != foundPlaylistsCollection)
				{
					foundPlaylistsCollection = value;
					OnPropertyChanged();
				}
			}
		}

		private ObservableCollection<Group> foundGroups = new ObservableCollection<Group>();
		private BoundObservableCollection<GroupViewModel, Group> foundGroupsCollection;
		public BoundObservableCollection<GroupViewModel, Group> FoundGroupsCollection
		{
			get { return foundGroupsCollection; }
			set
			{
				if (value != foundGroupsCollection)
				{
					foundGroupsCollection = value;
					OnPropertyChanged();
				}
			}
		}

		public SearchPageViewModel()
		{
			statusBar = StatusBar.GetForCurrentView().ProgressIndicator;
			foundSounds = new ObservableCollection<Sound>();
			FoundSoundsCollection = new BoundObservableCollection<SoundViewModel, Sound>(foundSounds,
				sound => new SoundViewModel(sound), (model, sound) => sound == model.model);

			foundUsers = new ObservableCollection<User>();
			FoundUsersCollection = new BoundObservableCollection<UserViewModel, User>(foundUsers,
				user => new UserViewModel(user, false), (model, sound) => sound == model.User);

			foundPlaylists = new ObservableCollection<Playlist>();
			FoundPlaylistsCollection = new BoundObservableCollection<PlaylistViewModel, Playlist>(foundPlaylists,
				playlist => new PlaylistViewModel(playlist), (model, playlist) => model.Playlist == playlist);

			foundGroups = new ObservableCollection<Group>();
			FoundGroupsCollection = new BoundObservableCollection<GroupViewModel, Group>(foundGroups,
				group => new GroupViewModel(group), (model, @group) => model.Group.ID == group.ID);
		}

		public ICommand StartSearch
		{
			get { return new RelayCommand(StartSearchExecute);}
		}

		public ICommand PlaySearch
		{
			get { return new RelayCommand(PlaySearchExecute);}
		}

		private void PlaySearchExecute(object param)
		{
			PlaylistManager.PlayList(foundSounds.ToList(), (int)param);
		}

		public async void StartSearchExecute(object param)
		{
			var vals = param as string[];

			if (vals != null)
			{
				await statusBar.ShowAsync();
				statusBar.Text = "Loading " + vals[0];
				var soundsCount = foundSounds.Count;
				var usersCount = foundUsers.Count;
				var playlistsCount = foundPlaylists.Count;
				var groupsCount = foundPlaylists.Count;
				Debug.WriteLine("Parsing results...");
				var result = await SoundCloudAPI.Search(vals[0], vals[1], 10);
				var objects = new JsonArray();
				if (!JsonArray.TryParse(result, out objects))
				{
					objects = new JsonArray();
				}
				foundUsers.Clear();
				foundSounds.Clear();
				foundPlaylists.Clear();
				foundGroups.Clear();
				foreach (var o in objects)
				{
					switch (vals[0])
					{
						case "tracks":
						{
							if (soundsCount == 0)
							{
								var sound = Sound.CreateFromJsonObject(o.GetObject());

								foundSounds.Add(sound);
							}
							//Debug.WriteLine("sound is " + o.Stringify());
							break;
						}
						case "users":
						{
							if (usersCount == 0)
							{
								var user = User.CreateUserFromJsonObject(o.GetObject());
								foundUsers.Add(user);
							}
							break;
						}
						case "playlists":
						{
							if (playlistsCount == 0)
							{
								Debug.WriteLine("read playlist");
								var playlist = Playlist.CreatePlaylistFromJsonObject(o.GetObject());
								foundPlaylists.Add(playlist);
								Debug.WriteLine("added playlist to list");
							}
							break;
						}
						case "groups":
						{
							if (groupsCount == 0)
							{
								var group = Group.CreateGroupFromJsonObject(o.GetObject());
								foundGroups.Add(group);
							}
							break;
						}
					}
					
				}
				await statusBar.HideAsync();
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
