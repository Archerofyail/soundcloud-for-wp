﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.Data.Json;
using Windows.UI.ViewManagement;
using Soundcloud_For_WP.Models;
using Soundcloud_For_WP.Shared.DataModels;

namespace Soundcloud_For_WP.Shared.ViewModels
{
	internal class MainPageViewModel : INotifyPropertyChanged
	{
		private User me;
		public ObservableCollection<Sound> streamSounds = new ObservableCollection<Sound>();
		public BoundObservableCollection<SoundViewModel, Sound> streamSoundViewModels;
		public ObservableCollection<MeSectionItemModel> meSectionList;
		private BoundObservableCollection<MeSectionItemViewModel, MeSectionItemModel> meSectionListCollection;
		private string NextStreamCollection;
		StatusBarProgressIndicator statusBar;
		public BoundObservableCollection<SoundViewModel, Sound> StreamSoundViewModels
		{
			get { return streamSoundViewModels; }
			set
			{
				if (streamSoundViewModels != value)
				{
					streamSoundViewModels = value;
					OnPropertyChanged();
				}
			}
		}

		public ObservableCollection<Sound> StreamSounds
		{
			get { return streamSounds; }
			set
			{
				if (value != streamSounds)
				{
					streamSounds = value;
					OnPropertyChanged();
				}
			}
		}

		public BoundObservableCollection<MeSectionItemViewModel, MeSectionItemModel> MeSectionList
		{
			get { return meSectionListCollection; }
			set
			{
				if (value != meSectionListCollection)
				{
					meSectionListCollection = value;

					OnPropertyChanged();
				}
			}
		}

		public async Task OnSuccessfulAuth()
		{
			var json = await SoundCloudAPI.GetMe();
			JsonObject userObj;
			JsonObject.TryParse(json, out userObj);
			if (userObj != null)
			{
				Debug.WriteLine("User Json:\n" + json);
				me = User.CreateUserFromJsonObject(userObj);
				Debug.WriteLine("Created user");
				meSectionList.Clear();

				meSectionList.Add(new MeSectionItemModel("Profile"));
				meSectionList.Add(new MeSectionItemModel("Now Playing"));
				meSectionList.Add(new MeSectionItemModel("Favourites", (me.publicFavouritesCount).ToString()));
				meSectionList.Add(new MeSectionItemModel("Playlists", me.playListCount.ToString()));
				meSectionList.Add(new MeSectionItemModel("Followings", me.followingsCount.ToString()));
				meSectionList.Add(new MeSectionItemModel("Followers", me.followersCount.ToString()));
			}
			else
			{
				meSectionList.Add(new MeSectionItemModel("", "Sorry, we can't sign in right now, try again later"));
			}
		}

		public ICommand Refresh
		{
			get { return new RelayCommand(RefreshExecute); }
		}

		public void RefreshExecute(object param)
		{
			CheckForSignIn();
		}

		public MainPageViewModel()
		{

			StreamSounds =
				new ObservableCollection<Sound>();
			meSectionList = new ObservableCollection<MeSectionItemModel>();
			meSectionListCollection = new BoundObservableCollection<MeSectionItemViewModel, MeSectionItemModel>(meSectionList,
				s => new MeSectionItemViewModel(s), (model, s) => model.Name == s.name);

			streamSoundViewModels = new BoundObservableCollection<SoundViewModel, Sound>(StreamSounds,
				(m) => { return new SoundViewModel(m); }, (vm, m) => { return (vm).model.ID == (m).ID; });
			statusBar = StatusBar.GetForCurrentView().ProgressIndicator;


			CheckForSignIn();
		}

		private async void CheckForSignIn()
		{
			try
			{
				await SoundCloudAPI.ReadToken();
			}
			catch (Exception)
			{

				Debug.WriteLine("not signed in");
				meSectionList.Add(new MeSectionItemModel("Sign In"));
			}

			if (SoundCloudAPI.isSignedIn)
			{
				statusBar.Text = "Signing in...";
				await statusBar.ShowAsync();
				await OnSuccessfulAuth();

				NextStreamCollection = "";
				statusBar.Text = "Loading Stream...";
				await LoadStream();
				await statusBar.HideAsync();
			}
			else
			{
				Debug.WriteLine("not signed in");
				meSectionList.Add(new MeSectionItemModel("Sign In"));
			}
		}

		private async Task LoadStream()
		{
			var json = await SoundCloudAPI.GetStream();
			JsonObject streamCollection;
			if (JsonObject.TryParse(json, out streamCollection) && streamCollection.ContainsKey("collection"))
			{

				IJsonValue tempVal;
				if (streamCollection.TryGetValue("next_href", out tempVal))
				{
					if (tempVal.ValueType == JsonValueType.String)
					{
						NextStreamCollection = tempVal.GetString();
					}
				}
				var tracksList = streamCollection.GetNamedArray("collection");
				int i = 0;
				foreach (var track in tracksList)
				{
					if (track.GetObject().GetNamedString("type") == "track")
					{
						var sound = Sound.CreateFromJsonObject(track.GetObject().GetNamedObject("origin"));
						streamSounds.Add(sound);
					}
					else if (track.GetObject().GetNamedString("type") == "track-sharing")
					{
						var sound = Sound.CreateFromJsonObject(track.GetObject().GetNamedObject("origin"));
						sound.UserName += "\n" + track.GetObject().GetNamedString("username");
						streamSounds.Add(sound);
						Debug.WriteLine("got shared track");
					}
					i++;
				}
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}