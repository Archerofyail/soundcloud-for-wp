﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using Soundcloud_For_WP.Models;

namespace Soundcloud_For_WP.Shared.ViewModels
{
	class MeSectionItemViewModel : INotifyPropertyChanged
	{
		private string name;
		private MeSectionItemModel model;
		public string Name
		{
			get { return name; }
			set
			{
				if (value != name)
				{
					name = value;
					OnPropertyChanged();
				}
			}
		}

		public string Extra
		{
			get { return model.extra; }
		}

		public MeSectionItemViewModel(string name)
		{
			this.name = name;
		}

		public MeSectionItemViewModel()
		{
			name = "Default";
		}

		public MeSectionItemViewModel(MeSectionItemModel source)
		{
			model = source;
			name = model.name;
		}

		public event PropertyChangedEventHandler PropertyChanged;

		
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			
			if (PropertyChanged != null) 
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
}
