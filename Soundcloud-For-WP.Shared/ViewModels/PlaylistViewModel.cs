﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Windows.Data.Json;
using Soundcloud_For_WP.Annotations;
using Soundcloud_For_WP.Shared;
using Soundcloud_For_WP.Shared.DataModels;
using Soundcloud_For_WP.Shared.Models;
using Soundcloud_For_WP.Shared.ViewModels;

namespace Soundcloud_For_WP.ViewModels
{
	internal class PlaylistViewModel : INotifyPropertyChanged
	{
		private Playlist playlist;
		private ObservableCollection<Sound> firstSounds = new ObservableCollection<Sound>();
		private BoundObservableCollection<SoundViewModel, Sound> firstSoundsCollection;

		private ObservableCollection<Sound> sounds;
		private BoundObservableCollection<SoundViewModel, Sound> soundsCollection;
		public Playlist Playlist
		{
			get { return playlist; }
			set
			{
				if (value != playlist)
				{
					playlist = value;
					foreach (var prop in GetType().GetRuntimeProperties())
					{
						if (prop.PropertyType == typeof (string) ||
						    prop.PropertyType == typeof (BoundObservableCollection<SoundViewModel, Sound>))
							OnPropertyChanged(prop.Name);
					}
				}
			}
		}

		public string Title
		{
			get { return playlist.Title; }
			set { OnPropertyChanged(); }
		}

		public string Creator
		{
			get { return playlist.User.GetNamedString("username"); }
			set { OnPropertyChanged(); }
		}

		public string Duration
		{
			get { return playlist.Duration.ToString("g"); }
			set { OnPropertyChanged(); }
		}

		public BoundObservableCollection<SoundViewModel, Sound> FirstSounds
		{
			get { return firstSoundsCollection; }
			set { OnPropertyChanged(); }
		}

		public BoundObservableCollection<SoundViewModel, Sound> Sounds
		{
			get { return soundsCollection; }
			set { OnPropertyChanged(); }
		}

		public string ArtworkUrl
		{
			get { return playlist.ArtworkUrl.Replace("large", "t300x300"); }
		}

		public string Likes
		{
			get { return " 0"; }
		}

		public string IsFavourite
		{
			get { return playlist.IsUserFavourite ? "" : ""; }
		}

		public string TrackCountAndLength
		{
			get { return playlist.Tracks.Length + " tracks, total time: " + playlist.Duration.ToString(@"hh\:mm\:ss"); }
		}

		public string Description
		{
			get { return playlist.Description; }
		}

		public ICommand FavouritePlaylist
		{
			get { return new RelayCommand(FavouritePlaylistExecute); }
		}

		public async void FavouritePlaylistExecute(object param)
		{
			if (playlist.IsUserFavourite)
			{
				await SoundCloudAPI.Like(playlist.ID);
				OnPropertyChanged("IsFavourite");
			}
			else
			{
				await SoundCloudAPI.Unlike(playlist.ID);
				OnPropertyChanged("IsFavourite");
			}
		}

		public PlaylistViewModel() {}

		public PlaylistViewModel(int id)
		{
			var jsonString = SoundCloudAPI.GetObjectById("playlists", id).ConfigureAwait(false).GetAwaiter().GetResult();
			try
			{
				Debug.WriteLine("playlist string is " + jsonString);

				playlist = Playlist.CreatePlaylistFromJsonObject(JsonObject.Parse(jsonString));
				for (var i = 0; i < (playlist.Tracks.Length <= 4 ? playlist.Tracks.Length : 5); i++)
				{
					firstSounds.Add(playlist.Tracks[i]);
				}
				sounds = new ObservableCollection<Sound>();
				foreach (var sound in playlist.Tracks)
				{
					sounds.Add(sound);
				}
				soundsCollection = new BoundObservableCollection<SoundViewModel, Sound>(sounds,
					playlist1 => new SoundViewModel(playlist1), (model, playlist1) => model.model.ID == playlist1.ID);
				firstSoundsCollection = new BoundObservableCollection<SoundViewModel, Sound>(firstSounds,
					sound => new SoundViewModel(sound), (model, sound) => model.model == sound);
			}
			catch (Exception e)
			{
				playlist = new Playlist();
				Debug.WriteLine("error, couldn't create playlist");
				Debug.WriteLine(e);
			}
		}

		public PlaylistViewModel(Playlist source)
		{
			if (source.Tracks.Length > 0)
			{
				for (var i = 0; i < (source.Tracks.Length <= 3 ? source.Tracks.Length : 3); i++)
				{
					firstSounds.Add(source.Tracks[i]);
				}
			}
			else
			{
				Debug.WriteLine("tracks length is zero");
			}
			sounds = new ObservableCollection<Sound>();
			foreach (var sound in source.Tracks)
			{
				sounds.Add(sound);
			}
			soundsCollection = new BoundObservableCollection<SoundViewModel, Sound>(sounds,
				playlist1 => new SoundViewModel(playlist1), (model, playlist1) => model.model.ID == playlist1.ID);
			firstSoundsCollection = new BoundObservableCollection<SoundViewModel, Sound>(firstSounds,
				sound => new SoundViewModel(sound), (model, sound) => model.model == sound);
			Playlist = source;
		}

		public event PropertyChangedEventHandler PropertyChanged;

		[NotifyPropertyChangedInvocator]
		private void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}