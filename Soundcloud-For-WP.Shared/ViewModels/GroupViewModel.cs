﻿using System.ComponentModel;
using System.Reflection;
using System.Runtime.CompilerServices;
using Windows.Data.Json;
using Soundcloud_For_WP.Annotations;
using Soundcloud_For_WP.Shared;
using Soundcloud_For_WP.Shared.Models;

namespace Soundcloud_For_WP.ViewModels
{
	
	sealed class GroupViewModel : INotifyPropertyChanged
	{
		private readonly Group group;

		public Group Group
		{
			get { return group; }
			set
			{
				if (group != value)
				{
					foreach (var prop in typeof (GroupViewModel).GetRuntimeProperties())
					{
						if (prop.PropertyType == typeof (string))
						{
							prop.SetMethod.Invoke(this, null);
						}
					}
				}
			}
		}

		public string Name
		{
			get { return group.Name; }
			set
			{
				OnPropertyChanged();
			}
		}

		public string ShortDescription
		{
			get { return group.ShortDescription; }
			set
			{
				OnPropertyChanged();
			}
		}

		public string ArtworkUrl
		{
			get { return group.ArtworkUrl; }
			set
			{
				OnPropertyChanged();
			}
		}

		public GroupViewModel()
		{
			
		}

		public GroupViewModel(int id)
		{
			group = Group.CreateGroupFromJsonObject(JsonObject.Parse(SoundCloudAPI.GetObjectById("group", id).Result));
		}

		public GroupViewModel(Group group)
		{
			this.group = group;

		}

		public event PropertyChangedEventHandler PropertyChanged;

		[NotifyPropertyChangedInvocator]
		private void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
