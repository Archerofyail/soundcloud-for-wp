﻿using Soundcloud_For_WP.Shared.Models;

namespace Soundcloud_For_WP
{
	static class SelectedPlaylist
	{
		public static Playlist Playlist { get; set; }
	}
}
