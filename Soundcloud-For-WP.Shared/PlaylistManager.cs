﻿using System.Collections.Generic;
using System.Diagnostics;
using Soundcloud_For_WP.Shared.DataModels;

namespace Soundcloud_For_WP.Shared
{
	public static class PlaylistManager
	{
		public static List<Sound> currentPlayList;
		private static int currentIndex;

		public static int Index
		{
			get { return currentIndex; }
		}

		static PlaylistManager()
		{
			currentPlayList = new List<Sound>();
		}

		public static void PlayList(List<Sound> sounds, int startIndex)
		{
			currentPlayList = new List<Sound>(sounds);
			currentIndex = startIndex;
		}

		public static void PlayListAndStartAtSongId(List<Sound> sounds, int songId)
		{
			var foundSound = sounds.Find(sound => sound.ID == songId);
			currentIndex = sounds.IndexOf(foundSound);
		}

		public static Sound GetCurrentSound()
		{
			return currentPlayList[currentIndex];
		}

		public static Sound MoveToPrevious()
		{
			currentIndex = currentIndex - 1 < 0 ? currentPlayList.Count - 1 : currentIndex - 1;
			return currentPlayList[currentIndex];
		}

		public static Sound MoveToNext()
		{
			currentIndex = currentIndex + 1 >= currentPlayList.Count ? 0 : currentIndex + 1;
			Debug.WriteLine("Moved index to next song");

			return currentPlayList[currentIndex];
		}
	}
}
