﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using Windows.Foundation.Collections;
using Windows.Phone.UI.Input;
using Windows.System;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556
using Soundcloud_For_WP.Shared;
using Soundcloud_For_WP.Shared.DataModels;
using Soundcloud_For_WP.Shared.Models;
using Soundcloud_For_WP.Shared.ViewModels;
using Soundcloud_For_WP.ViewModels;

namespace Soundcloud_For_WP
{
	/// <summary>
	/// An empty page that can be used on its own or navigated to within a Frame.
	/// </summary>
	public sealed partial class SearchPage : Page
	{
		private List<string> searchedTypes = new List<string>();
		public SearchPage()
		{
			this.InitializeComponent();
			NavigationCacheMode = NavigationCacheMode.Required;
			Window.Current.CoreWindow.KeyDown += OnReSearch;
		}

		public void OnBackPressed(object sender, BackPressedEventArgs backPressedEventArgs)
		{

			if (Frame.CanGoBack)
			{
				Frame.GoBack();
				backPressedEventArgs.Handled = true;
				Debug.WriteLine("went back");
			}
			Debug.WriteLine("Called on back pressed");
			backPressedEventArgs.Handled = true;
			HardwareButtons.BackPressed -= OnBackPressed;


		}

		public async void OnReSearch(CoreWindow sender, KeyEventArgs args)
		{
			var searchType = (SearchSectionsPivot.SelectedItem as PivotItem).Header.ToString().ToLower();
			var searchString = searchBox.Text;
			if (args.VirtualKey == VirtualKey.Enter)
			{
				searchBox.IsEnabled = false;
				if (!string.IsNullOrEmpty(searchString))
				{
					await Dispatcher.RunAsync(CoreDispatcherPriority.High, () =>
					{
						(DataContext.GetType().GetRuntimeProperty("StartSearch").GetValue(DataContext) as RelayCommand).Execute(new[]
						{searchType, searchString});
					});
					searchedTypes.Clear();
					searchedTypes.Add(searchType);
					
				}

				searchBox.IsEnabled = true;
			}

		}

		/// <summary>
		/// Invoked when this page is about to be displayed in a Frame.
		/// </summary>
		/// <param name="e">Event data that describes how this page was reached.
		/// This parameter is typically used to configure the page.</param>
		protected override void OnNavigatedTo(NavigationEventArgs e)
		{
			HardwareButtons.BackPressed += OnBackPressed;
		}

		private async void SectionChanged(object sender, SelectionChangedEventArgs e)
		{
			Debug.WriteLine("start selection changed method");
			var searchType = (SearchSectionsPivot.SelectedItem as PivotItem).Header.ToString().ToLower();
			var searchString = searchBox.Text;
			var senderContent = (SearchSectionsPivot.SelectedItem as PivotItem).Content;
			Debug.WriteLine("content is " + senderContent);
			var doesListExist = (senderContent as ListView).Items.Count > 0;
			if (!string.IsNullOrEmpty(searchString) && (!searchedTypes.Contains(searchType) || !doesListExist))
			{
				searchedTypes.Add(searchType);
				await Dispatcher.RunAsync(CoreDispatcherPriority.Low, () =>
				{
					(DataContext as SearchPageViewModel).StartSearch.Execute(new[] { searchType, searchString });
				});
				
			}
			Debug.WriteLine("End selection changed method");
		}

		private async void Tracks_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			var listView = (sender as ListView);
			var list = (listView.ItemsSource as BoundObservableCollection<SoundViewModel, Sound>)._source.ToList();
			Debug.WriteLine("type of items source is " + listView.ItemsSource.GetType());
			PlaylistManager.PlayList(list, listView.SelectedIndex);
			Frame.Navigate(typeof(PlayingSoundView));
		}

		private async void Users_ItemSelected(object sender, SelectionChangedEventArgs selectionChangedEventArgs)
		{
			var userViewModel = (users.ItemsSource as BoundObservableCollection<UserViewModel, User>)[users.SelectedIndex];
			var data = new ValueSet();
			data.Add("user", userViewModel.User.id);
			Frame.Navigate(typeof(UserPage), data);
		}

		private async void Playlists_ItemSelected(object sender, SelectionChangedEventArgs selectionChangedEventArgs)
		{
			var itemsSource = (Playlists.ItemsSource as BoundObservableCollection<PlaylistViewModel, Playlist>);
			if (Playlists.SelectedIndex >= itemsSource.Count || itemsSource.Count <= 0) return;
			SelectedPlaylist.Playlist = (itemsSource[Playlists.SelectedIndex]).Playlist;
			Frame.Navigate(typeof(PlaylistPage));
		}

		private void Groups_ItemSelected(object sender, SelectionChangedEventArgs selectionChangedEventArgs)
		{
			Frame.Navigate(typeof(GroupPage), ((sender as ListView).SelectedItem as Group).ID);
		}

		private async void OnRefreshTapped(object sender, TappedRoutedEventArgs e)
		{
			var searchType = (SearchSectionsPivot.SelectedItem as PivotItem).Header.ToString().ToLower();
			var searchString = searchBox.Text;

			searchBox.IsEnabled = false;
			if (!string.IsNullOrEmpty(searchString))
			{
				await Dispatcher.RunAsync(CoreDispatcherPriority.High, () =>
				{
					(DataContext.GetType().GetRuntimeProperty("StartSearch").GetValue(DataContext) as RelayCommand).Execute(new[] { searchType, searchString });
				});
				searchedTypes.Clear();
				searchedTypes.Add(searchType);
			}

			searchBox.IsEnabled = true;
		}
	}
}
