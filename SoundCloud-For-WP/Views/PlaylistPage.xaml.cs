﻿using System.Linq;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using Soundcloud_For_WP.Shared;
using Soundcloud_For_WP.Shared.DataModels;
using Soundcloud_For_WP.Shared.ViewModels;
using Soundcloud_For_WP.ViewModels;

namespace Soundcloud_For_WP
{
	public sealed partial class PlaylistPage : Page
	{
		public PlaylistPage()
		{
			InitializeComponent();
		}

		protected override void OnNavigatedTo(NavigationEventArgs e)
		{
			DataContext = new PlaylistViewModel(SelectedPlaylist.Playlist);
		}

		private void Selector_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			PlaylistManager.PlayList(
				((sender as ListView).ItemsSource as BoundObservableCollection<SoundViewModel, Sound>)._source.ToList(),
				(sender as ListView).SelectedIndex);
			Frame.Navigate(typeof (PlayingSoundView));
		}
	}
}