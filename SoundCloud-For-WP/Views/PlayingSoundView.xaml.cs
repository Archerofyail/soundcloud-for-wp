﻿using System;
using System.Diagnostics;
using Windows.Foundation.Collections;
using Windows.Media.Playback;
using Windows.Phone.UI.Input;
using Windows.UI.Core;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556
using Soundcloud_For_WP.Shared;
using Soundcloud_For_WP.Shared.ViewModels;

namespace Soundcloud_For_WP
{
	/// <summary>
	/// An empty page that can be used on its own or navigated to within a Frame.
	/// </summary>
	public sealed partial class PlayingSoundView : Page
	{
		private bool isTaskRunning;
		private MediaPlayer player;
		public PlayingSoundView()
		{
			this.InitializeComponent();
			this.NavigationCacheMode = NavigationCacheMode.Required;
			player = BackgroundMediaPlayer.Current;
			HardwareButtons.BackPressed += BackPressed;
			BackgroundMediaPlayer.MessageReceivedFromBackground += MessageReceived;
		}

		public string IsPlaying
		{
			get { return BackgroundMediaPlayer.IsMediaPlaying() ? "" : ""; }
		}

		private async void MessageReceived(object sender, MediaPlayerDataReceivedEventArgs e)
		{
			foreach (var key in e.Data.Keys)
			{
				switch (key)
				{
					case "SendData":
					{
						OnTaskReady();
						Debug.WriteLine("foreground was told to send data");
						isTaskRunning = true;
						break;
					}
					case "movetoprevious":
					{

						await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
						{
							NextButton.IsEnabled = true;
							PrevButton.IsEnabled = true;
							PlayButton.IsEnabled = true;
							(DataContext as PlayingSoundViewModel).MoveToPrevious.Execute(this);
						});
						break;
					}
					case "movetonext":
					{

						await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
						{
							NextButton.IsEnabled = true;
							PrevButton.IsEnabled = true;
							PlayButton.IsEnabled = true;
							(DataContext as PlayingSoundViewModel).MoveToNext.Execute(this);
						});
						break;
					}
					case "isPlaying":
					{
						if ((bool)e.Data[key])
						{
							await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
							{

								PlayButton.Content = (BackgroundMediaPlayer.IsMediaPlaying() ? "" : "");
							});
						}
						break;
					}
					case "playing":
					{
						string key1 = key;
						await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
						{
							NextButton.IsEnabled = true;
							PrevButton.IsEnabled = true;
							PlayButton.IsEnabled = true;
							if ((bool)e.Data[key1])
							{
								PlayButton.Content = "";
							}
							else
							{
								PlayButton.Content = "";
							}
						});

						
						break;
					}
				}
			}
		}

		protected override void OnNavigatedTo(NavigationEventArgs e)
		{
			base.OnNavigatedTo(e);
			DataContext = new PlayingSoundViewModel(PlaylistManager.GetCurrentSound());
			if (isTaskRunning)
			{
				if (e.Parameter is bool)
				{
					if ((bool)e.Parameter)
					{

					}
					Debug.WriteLine("task was already running, not sending new data");
				}
				else
				{
					Debug.WriteLine("task was already running, sending new data");
					OnTaskReady();
				}

			}
		}

		private async void OnTaskReady()
		{
			await Dispatcher.RunAsync(CoreDispatcherPriority.Low, () =>
			{
				var data = new ValueSet();
				int i = 0;
				foreach (var sound in PlaylistManager.currentPlayList)
				{
					data.Add("streamurl" + i, sound.StreamUrl);
					data.Add("title" + i, sound.Title);
					data.Add("artist" + i, sound.User.GetNamedString("username"));
					i++;
				}
				data.Add("index", PlaylistManager.Index);
				BackgroundMediaPlayer.SendMessageToBackground(data);
				Debug.WriteLine("sent message to background");
			});
		}

		private void BackPressed(object sender, BackPressedEventArgs e)
		{
			if (Frame.CanGoBack && !e.Handled)
			{
				Frame.GoBack();
				e.Handled = true;
			}
		}

		public async void MoveToPreviousExecute(object param, TappedRoutedEventArgs tappedRoutedEventArgs)
		{
			NextButton.IsEnabled = false;
			PrevButton.IsEnabled = false;
			PlayButton.IsEnabled = false;
			await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
			{
				var value = new ValueSet();
				value.Add("movetoprevious", "");
				BackgroundMediaPlayer.SendMessageToBackground(value);
			});

		}

		public async void MoveToNextExecute(object param, TappedRoutedEventArgs tappedRoutedEventArgs)
		{
			NextButton.IsEnabled = false;
			PrevButton.IsEnabled = false;
			PlayButton.IsEnabled = false;
			await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
			{
				var value = new ValueSet();
				value.Add("movetonext", "");
				BackgroundMediaPlayer.SendMessageToBackground(value);
			});
		}

		public async void PauseResumeExecute(object sender, TappedRoutedEventArgs tappedRoutedEventArgs)
		{
			var data = new ValueSet { { "play", MediaPlayerState.Paused == BackgroundMediaPlayer.Current.CurrentState } };
			BackgroundMediaPlayer.SendMessageToBackground(data);
			await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
			{
				(sender as Button).IsEnabled = false;
			});
		}

	}
}
