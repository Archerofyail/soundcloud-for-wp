﻿using System;
using System.Diagnostics;
using System.Linq;
using Windows.Data.Json;
using Windows.Foundation.Collections;
using Windows.System;
using Windows.UI.Core;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556
using Soundcloud_For_WP.Shared;
using Soundcloud_For_WP.Shared.DataModels;
using Soundcloud_For_WP.Shared.Models;
using Soundcloud_For_WP.Shared.ViewModels;
using Soundcloud_For_WP.ViewModels;

namespace Soundcloud_For_WP
{
	/// <summary>
	/// An empty page that can be used on its own or navigated to within a Frame.
	/// </summary>
	public sealed partial class UserPage : Page
	{
		public UserPage()
		{
			this.InitializeComponent();
		}

		/// <summary>
		/// Invoked when this page is about to be displayed in a Frame.
		/// </summary>
		/// <param name="e">Event data that describes how this page was reached.
		/// This parameter is typically used to configure the page.</param>
		protected async override void OnNavigatedTo(NavigationEventArgs e)
		{
			var data = e.Parameter as ValueSet;
			UserViewModel userViewModel;
			var progressIndicator = StatusBar.GetForCurrentView().ProgressIndicator;
			await progressIndicator.ShowAsync();
			progressIndicator.Text = "Loading...";
			if (data.ContainsKey("me"))
			{

				await Dispatcher.RunAsync(CoreDispatcherPriority.Low, () =>
				{
					Debug.WriteLine("data contains me, grabbing my user profile...");
					var jsonstring = SoundCloudAPI.GetMe("").Result;
					Debug.WriteLine("got me, json is " + jsonstring);
					var me = User.CreateUserFromJsonObject(JsonObject.Parse(jsonstring));
					userViewModel = new UserViewModel(me, true);
					ProfilePivot.SelectedIndex = (int) data["me"];
					DataContext = userViewModel;
				});
				

			}
			else
			{
				await Dispatcher.RunAsync(CoreDispatcherPriority.Low, () =>
				{
					userViewModel = new UserViewModel((int) data["user"]);
					DataContext = userViewModel;
				});
				
			}
			await progressIndicator.HideAsync();

		}

		private async void WebsiteButton_OnTap(object sender, TappedRoutedEventArgs args)
		{
			await Launcher.LaunchUriAsync(new Uri((DataContext as UserViewModel).Website));
		}

		private void Tracks_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			PlaylistManager.PlayList(
				((sender as ListView).ItemsSource as BoundObservableCollection<SoundViewModel, Sound>)._source.ToList(),
				(sender as ListView).SelectedIndex);
			Frame.Navigate(typeof(PlayingSoundView));
		}

		private void Playlists_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			var itemsSource = (Playlists.ItemsSource as BoundObservableCollection<PlaylistViewModel, Playlist>);
			var playlistViewModel = (itemsSource[Playlists.SelectedIndex]);
			SelectedPlaylist.Playlist = playlistViewModel.Playlist;
			Frame.Navigate(typeof(PlaylistPage));
		}

		private void Users_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			Frame.Navigate(typeof (UserPage),
				((sender as ListView).ItemsSource as BoundObservableCollection<UserViewModel, User>)._source[
					(sender as ListView).SelectedIndex]);
			;
		}
	}
}
