﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Net;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;
using Soundcloud_For_WP.Models;
using Soundcloud_For_WP.Shared;
using Soundcloud_For_WP.Shared.DataModels;
using Soundcloud_For_WP.Shared.ViewModels;

namespace Soundcloud_For_WP
{
	public sealed partial class MainPage : Page
	{

		public MainPage()
		{
			InitializeComponent();

			NavigationCacheMode = NavigationCacheMode.Required;

		}

		protected override void OnNavigatedTo(NavigationEventArgs e)
		{
			var parameters = e.Parameter as string;
			if (string.IsNullOrEmpty(parameters))
			{
				DataContext = new MainPageViewModel();
				return;
			}
			var paramString = new Uri(parameters);
			var components = paramString.GetComponents(UriComponents.Query, UriFormat.Unescaped);
			Debug.WriteLine("Got query component of the uri with text " + components);
			var code = components.Substring(5);
			SignIn(code);
		}

		private async void SignIn(string code)
		{
			await SoundCloudAPI.OAuthenticate(code);
			DataContext = new MainPageViewModel();
		}

		private void StreamView_OnTapped(object sender, TappedRoutedEventArgs e)
		{
			var listView = (sender as ListView);
			var list = (listView.ItemsSource as BoundObservableCollection<SoundViewModel, Sound>)._source.ToList();
			PlaylistManager.PlayList(list, listView.SelectedIndex);
			Frame.Navigate(typeof(PlayingSoundView));
		}

		private void OnSearchTapped(object sender, RoutedEventArgs routedEventArgs)
		{
			Frame.Navigated += OnSuccessfulNavigation;
			Frame.Navigate(typeof(SearchPage));
		}

		private void OnSuccessfulNavigation(object sender, NavigationEventArgs navigationEventArgs)
		{
			Debug.WriteLine("sender type is " + (navigationEventArgs.Content as Page).GetType());
			var nextPage = navigationEventArgs.Content as SearchPage;
			nextPage.Frame.BackStack.Add(new PageStackEntry(typeof(MainPage), this, new CommonNavigationTransitionInfo()));
			Frame.Navigated -= OnSuccessfulNavigation;
		}

		private void MeSectionList_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			Debug.WriteLine("sender is " + sender);
			var selectedItemName =
				((sender as ListView).ItemsSource as BoundObservableCollection<MeSectionItemViewModel, MeSectionItemModel>)[
					(sender as ListView).SelectedIndex].Name;
			Debug.WriteLine("Name is " + selectedItemName);
			var data = new ValueSet();
			switch (selectedItemName)
			{
				case "Sign In":
				{
					SoundCloudAPI.SignIn();
					break;
				}
				case "Now Playing":
				{
					Frame.Navigate(typeof(PlayingSoundView));
					break;
				}
				case "Profile":
				{
					Debug.WriteLine("Going to profile...");
					data.Add("me", 0);
					Frame.Navigate(typeof(UserPage), data);
					break;
				}
				case "Favourites":
				{
					data.Add("me", 3);
					Frame.Navigate(typeof(UserPage), data);
					break;
				}
				case "Followers":
				{
					data.Add("me", 4);
					Frame.Navigate(typeof(UserPage), data);
					break;
				}
				case "Following":
				{
					data.Add("me", 5);
					Frame.Navigate(typeof(UserPage), data);
					break;
				}
				case "Tracks":
				{
					data.Add("me", 1);
					Frame.Navigate(typeof(UserPage), data);
					break;
				}
				case "Playlists":
				{
					data.Add("me", 2);
					Frame.Navigate(typeof(UserPage), data);
					break;
				}
			}
		}

		private void CommandBar_OnItemTapped(object sender, TappedRoutedEventArgs e)
		{
			var button = sender as AppBarButton;
			switch (button.Label)
			{
				case "About":
				{
					Frame.Navigate(typeof(AboutPage));
					break;
				}
				case "Settings":
				{
					Frame.Navigate(typeof(SettingsPage));
					break;
				}
			}
		}
	}
}
