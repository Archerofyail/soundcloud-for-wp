﻿using Windows.Data.Json;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556
using Soundcloud_For_WP.Shared;
using Soundcloud_For_WP.Shared.DataModels;
using Soundcloud_For_WP.Shared.ViewModels;

namespace Soundcloud_For_WP
{
	/// <summary>
	/// An empty page that can be used on its own or navigated to within a Frame.
	/// </summary>
	public sealed partial class TrackInfoPage : Page
	{
		public TrackInfoPage()
		{
			this.InitializeComponent();
		}

		/// <summary>
		/// Invoked when this page is about to be displayed in a Frame.
		/// </summary>
		/// <param name="e">Event data that describes how this page was reached.
		/// This parameter is typically used to configure the page.</param>
		protected override void OnNavigatedTo(NavigationEventArgs e)
		{
			if (e.Parameter is int)
			{
				var trackId = (int) e.Parameter;
				DataContext =
					new SoundViewModel(
						Sound.CreateFromJsonObject(JsonObject.Parse(SoundCloudAPI.GetObjectById("track", trackId).Result)));
			}
		}

		private void OnAppBarItemTapped(object sender, TappedRoutedEventArgs args)
		{
			var senderLabel = (sender as AppBarButton).Label;
			switch (senderLabel)
			{
				case "Favourite":
				{
					break;
				}
				case "Repost":
				{
					break;
				}
				case "Share":
				{
					break;
				}
				case "Download":
				{
					break;
				}
				case "Add to Playlist":
				{
					break;
				}
			}
		}
	}
}